## Archive description 
This archive contains raw data export of adaptiv' math student xapi statement, a csv export, some documentation and a python script to parse and process the raw statements to generate organised and clean student sequence of exercises and store then in new files. 

To dive directly into the parsing process, you can just install the requirements and launch the jupyter notebook Get_started.ipynb. 
```
pip install requirements.txt
jupyter notebook Get_started.ipynb. 
```
Other wise you can explore the different directories.

### Detailed list of folder and files:

* README.md: this file
* requirements.txt: python lib required to run genStudSequence.py 
* genStudSequence.py: contains all parsing functions to organise the data into student sequence of exercises
* Get_started.ipynb: jupyter notebook to start the parsing process and understand how to use the data

* Documentation/ 
    * raw_file_json_doc/ 
        * datajson_doc.json: raw documentation of a reworked example of dataAdMaths.json file
        * statements_doc.json: raw documentation of the xapi statements
    
    * graph_representations/
        * graphAm_Mx_falt.pdf: flat representation of module Mx  
        * graphAm_Mx.pdf: exploded representation of module Mx
    
    * Pedagogical_Structure_Explanation.ipynb: jupyter notebook to explain the pedagogical structure of Adaptiv' Math exercises
    * Documentation_Json_Files.ipynb: jupyter notebook to explain json files structure
    * Adaptive_curriculum_paper.pdf: scientifique paper about the algorithm behind the curriculum personnalization 

* Data/
    * config_files/  
        * allCodeId_AM.json: traduction from alpha numeric item Ids to "Mx\*Oy\*Az" code style
        * dataAdMaths.json: description of the pedagogical data used in the app (raw information about module, obj, act, exexercises descriptions) 

    * mongoexport/
        * student_exercise_statements.json: raw export of student exercise xapi statements 
        * other_student_statements.json: raw export of other student xapi statements (not really relevant)

    * csvExport/ 
        * donnees_admath_men.csv: export from the raw statement in csv format
        * csv_export_doc.md: documentation of csv export collumn
    
    * studSequences/
        * compiledStudentSeqFiles/
            * all final files containing student exercises sequences generated during the parsing process are stored here
        * NO_RAM_all_student_intermediary_files/
            * directory will be created and all temporary/intermediary files generated during the low RAM consuming parsing process will be stored here
        * With_RAM_all_student_intermediary_files/
            * directory will be created and the temporary/intermediary file generated during the high RAM consuming parsing process wiil be stored here
