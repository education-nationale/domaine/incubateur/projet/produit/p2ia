import os
import tqdm
import json
import ijson
import argparse
import datetime

# Raw Statement Paths
FileName = "student_exercise_statements.json"
LoadingPath = "Data/mongoexport/"

# Config files paths
ConfigFiles_path = "Data/config_files/"
MapCodeIdsFileName = "allCodeId_AM.json"
MapCodeIdsFilePath = os.path.join(ConfigFiles_path, MapCodeIdsFileName)

# Path to Data file which contains data description (titles, module, obj, act, exexercises descriptions) 
DataDescriptionFileName = "dataAdMaths.json"
DataDescriptionFilePath = os.path.join(ConfigFiles_path, DataDescriptionFileName)

# File to write list of student Ids
StudentIdListFileName = "listOfStudentId.json"

# Student Sequence Dir paths
StudSeqDir = "Data/studSequences/"

TempNoRamAllFilesDir = "NO_RAM_all_student_intermediary_files/" # used if no RAM 
TempWithRamFileDir = "With_RAM_all_student_intermediary_files/" # used if use RAM 
CompiledStudSeqDir = "compiledStudentSeqFiles/"

# Student Sequence File Names
TempAllStudSeqFileName = "temp_all_Student_Sequences.json" # used if RAM 
AllStudSeqFileName = "all_Student_Sequences.json"
AllSimpleStudSeqFileName = "all_simpleSequences.json"

# Date Format
Date_format = "%Y-%m-%dT%H:%M:%S.%fZ"

# Load mapping between alphaNum activity ID and redable codes (MxOyAz => Module x Objectif y, Activity z) 
with open(MapCodeIdsFilePath, "r") as mapCodeIdFile:
    MapCodeIdAD = json.loads(mapCodeIdFile.read())

# Load Raw Data Descriptions 
with open(DataDescriptionFilePath, "r") as DataDescriptionFile:
    RawDataDescription = json.loads(DataDescriptionFile.read())

# Init what type of sequences we want
TypeOfSeqData = ["initialTest", "adaptivCuriculum", "simpleAdaptivCuriculum"]

def load_students_seqs(pathRef=StudSeqDir, perLineFile=True, moduleIdx=1, typeDataIdx=2):
    """ Load sequence student seq Data

    Parameters
    ----------
    pathRef : str
        the path of the root student sequence file directory 

    perLineFile: bool

        if it is a file writen perLine or not

    moduleIdx: int

        module index : 1 , 31, 32, 33, 41, 42, 43

    typeOfDataIdx: int
        index of type of data: 0: initialTest, 1: adaptivCuriculum, 2: simpleAdaptivCuriculum

    Returns
    -------
    list of dict
        list of student sequences
    """

    moduleCodeId = f"M{moduleIdx}"
    typeData = TypeOfSeqData[typeDataIdx]
    # Path for the file we will put all the simple student sequences into
    moduleAllStudSeqDirPath = os.path.join(pathRef,CompiledStudSeqDir,moduleCodeId)
    typeDataStudeSeqFilePath = os.path.join(moduleAllStudSeqDirPath, f"{moduleCodeId}_{typeData}.json")              
    simpleStudData = []
    with open(typeDataStudeSeqFilePath, "r") as typeDataStudeSeqFile:
        if(perLineFile):
            for line in typeDataStudeSeqFile:
                simpleStudData.append(json.loads(line))
        else:
            simpleStudData = json.loads(typeDataStudeSeqFile.read())
    return simpleStudData

def main(processToLaunch=2, no_RAM=True, delStudFiles=True):
    """ Launch full process, parse raw statement to general student sequences files, then compile
    them all again in two files with all students data parsed and well organised

    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--processToLaunch', 
        help="What process do you want to launch: 2 for both statement parsing and unique file gen, 1 : only statement parsing, 0: only unique file gen",
        default=processToLaunch, 
        required=False, 
        choices=["2", "1", "0"])

    parser.add_argument(
        '--no_RAM', 
        help='Def if you don\'t want to exploit RAM (at least 6GO needed otherwise) and generate 60k+ files, True by default', 
        default=no_RAM, 
        required=False, 
        choices=["0", "1"])

    parser.add_argument(
        '--delStudFiles', 
        help='Do you want to delete student files if no_RAM is True, False by default', 
        default=delStudFiles, 
        required=False, 
        choices=["0", "1"])

    args = parser.parse_args(args=[])

    no_RAM = bool(int(args.no_RAM))
    delStudFiles = bool(int(args.delStudFiles)) 
    processToLaunch = int(args.processToLaunch)

    if(processToLaunch in [2,0]):
        print("Parse raw statement to generate clean sequence json entries")
        if(no_RAM):
            print("This process will generate 60k+ files \n")
        else:
            print("More than 4GO+ RAM will be used in this process \n")
        generate_student_sequences(no_Ram=no_RAM)
        print("")
        
    if(processToLaunch in [2,1]):
        print("Compile generated students files into two files empassing all ordered and cleaned student data")
        if(no_RAM):
            if(delStudFiles):
                print("The 60k+ files generated WILL BE deleted during the process")
            else:
                print("The 60k+ files generated WILL NOT BE deleted during the process")
        else:
            print("More than 2GO+ RAM will be used in this process")

        compileStudFileIntoOne(deleteFiles=args.delStudFiles, no_Ram=no_RAM)
        print("")


def compileStudFileIntoOne(pathRef=StudSeqDir, allFileDirectory=TempNoRamAllFilesDir, deleteFiles=False, no_Ram=True):
    """ Compile all stundent files into two file 
    one with all sequence informations, ordered by date and one with simplified sequences also ordered
    The struture of the Data are in : 
        Full data file : 

        Simplified data file:
  
    Parameters
    ----------
    pathRef : str
        the path of the root student sequence file directory 

    allFileDirectory : str
        the name of the directory with all the student files

    """

    # Path for all student sequence files
    noRamAllStudFilesPath = os.path.join(pathRef, allFileDirectory)

    # Get list of student Id
    studentIDList = []
    studentIdListFilePath = os.path.join(StudSeqDir, CompiledStudSeqDir, StudentIdListFileName)
    with open(studentIdListFilePath, "r") as studentIdListFile:
        for studentId in studentIdListFile:
            studentIDList.append(str(studentId).split("\n")[0])
    
    # get dirPath path and total size depending of RAM consuming process chosen 
    if(no_Ram):
        dirRefTempStudDataFilePath = noRamAllStudFilesPath
        totalSize = get_dir_size(dirRefTempStudDataFilePath)
    
    else:
        dirRefTempStudDataFilePath = os.path.join(pathRef, TempWithRamFileDir)
        tempWithRamStudSeqFilePath = os.path.join(dirRefTempStudDataFilePath, TempAllStudSeqFileName)
        with open(tempWithRamStudSeqFilePath, "r") as tempAllStudSeqFile:
            tempAllStudata = json.loads(tempAllStudSeqFile.read())
        totalSize = int(len(json.dumps(tempAllStudata))*0.99) # Small offset of progress bar

    #Init list of modules
    listModuleCodeId = []
    listStudFilePath = []

    # Process to compile the files
    with tqdm.tqdm(total=totalSize) as pbar:
        for nStud, studentId in zip(range(len(studentIDList)), studentIDList):
            studData = {
                "nbAdaptivExerciseDone": {}
            }

            for typeData in TypeOfSeqData:
                studData[typeData] = {}

            # Treat to put stud input into right module
            def treatLineData(studData, lineData):
                if(typeData == "tutorialVideo"):
                    studData[typeData].append(lineData)
                else:
                    labelCodeID = {"adaptivCuriculum": "codeActivity", "initialTest": "codeActivity", "simpleAdaptivCuriculum":"act"}[typeData]
                    moduleCodeId = lineData[labelCodeID].split("O")[0]
                    if(moduleCodeId not in listModuleCodeId): 
                        listModuleCodeId.append(moduleCodeId)
                    if(moduleCodeId not in studData[typeData].keys()):
                        studData[typeData][moduleCodeId] = []    
                    studData[typeData][moduleCodeId].append(lineData)

            # For each type of sequence Data, update the dictionnary from student dedicated file            
            for typeData in TypeOfSeqData:
                if(no_Ram):
                    studFileName = f"{studentId}_{typeData}.json"
                    studFilePath = os.path.join(noRamAllStudFilesPath, studFileName)
                    with open(studFilePath, "r") as studFile:
                        for line in studFile:
                            lineData = json.loads(line)
                            treatLineData(studData, lineData)
                            pbar.update(len(line))
                else:
                    tempTypedStudData = tempAllStudata[studentId][typeData]
                    for lineData in tempTypedStudData:
                        treatLineData(studData, lineData)
                        pbar.update(len(json.dumps(lineData)))

                if(typeData == "simpleAdaptivCuriculum"):
                    for moduleId, moduleSeq in studData[typeData].items():

                        studData["nbAdaptivExerciseDone"][moduleId] = len(moduleSeq)
                
                # Delete students files if needed 
                if(deleteFiles and no_Ram):
                    os.remove(studFilePath)

            # Order by date
            for typeData, data in studData.items():
                if typeData is TypeOfSeqData:
                    for moduleData in data.values():
                        moduleData.sort(key=lambda x: datetime.datetime.strptime(x["date"], Date_format))
                elif typeData == "tutorialVideo":
                    studData[typeData].sort(key=lambda x: datetime.datetime.strptime(x["date"], Date_format))

            # Init simpleData
            simpleData = studData["simpleAdaptivCuriculum"]
            
            # Add student file content to global files
            for moduleCodeId, nbEx in studData["nbAdaptivExerciseDone"].items():
                moduleStudData = {
                    "studentdId": studentId,
                    "nbAdaptivExerciseDone": nbEx
                }
                # Path for the directoy for the files we will put all the student sequences data into
                moduleAllStudSeqDirPath = os.path.join(pathRef,CompiledStudSeqDir,moduleCodeId)
                if not os.path.exists(moduleAllStudSeqDirPath):
                    os.makedirs(moduleAllStudSeqDirPath)

                for typeData in TypeOfSeqData:
                    if(moduleCodeId in studData[typeData].keys()):
                        moduleStudData[typeData] = studData[typeData][moduleCodeId]
                    else:
                        moduleStudData[typeData] = []

                    # Path for the file we will put all the type data student sequences into
                    moduleAllTypeDataStudeSeqFilePath = os.path.join(moduleAllStudSeqDirPath, f"{moduleCodeId}_{typeData}.json")
                    if moduleAllTypeDataStudeSeqFilePath not in listStudFilePath:
                        with open(moduleAllTypeDataStudeSeqFilePath, "w") as moduleAllTypeDataStudeSeqFile:
                            moduleAllTypeDataStudeSeqFile.write("")
                        listStudFilePath.append(moduleAllTypeDataStudeSeqFilePath)

                    # Add type data sequence student content to type data global file
                    with open(moduleAllTypeDataStudeSeqFilePath, "a") as moduleAllTypeDataStudeSeqFile:
                        if(typeData == "simpleAdaptivCuriculum"):
                            for ex in moduleStudData[typeData]:
                                # remove the date to reduce size even more for simplified sequence, it has been ordered, the index serve as time reference
                                del ex["date"]
                        moduleAllTypeDataStudeSeqFile.write(json.dumps(moduleStudData[typeData]))
                        moduleAllTypeDataStudeSeqFile.write("\n")
                
                # Path for the file we will put all the student sequences into
                moduleAllStudeSeqFilePath = os.path.join(moduleAllStudSeqDirPath, f"{moduleCodeId}_{AllStudSeqFileName}")
                if moduleAllStudeSeqFilePath not in listStudFilePath:
                    with open(moduleAllStudeSeqFilePath, "w") as moduleAllStudeSeqFile:
                        moduleAllStudeSeqFile.write("")
                    listStudFilePath.append(moduleAllStudeSeqFilePath)

                with open(moduleAllStudeSeqFilePath, "a") as moduleAllStudeSeqFile:
                    moduleAllStudeSeqFile.write(json.dumps(moduleStudData))
                    moduleAllStudeSeqFile.write("\n")
        
    # Delete intermediary file if needed 
    if(deleteFiles and not no_Ram):
        os.remove(tempAllStudata)



def generate_student_sequences(filePerLine=True, no_Ram=True):
    """ Generate the exercises sequence files for each student 
    The files contains the processed sequence of exercises for each student from the raw statements 

    Parameters
    ----------
    filePerLine : bool
        is the student file a one line per statement file

    """

    # init directory and files paths
    statementsFile_name = FileName.split(".")[0] + ".json"
    statementsPath = os.path.join(LoadingPath, statementsFile_name)
    
    if(no_Ram):
        noRamAllStudFilesPath = os.path.join(StudSeqDir, TempNoRamAllFilesDir)
        if not os.path.exists(noRamAllStudFilesPath):
            os.makedirs(noRamAllStudFilesPath)
            
    else:
        tempWithRamStudSeqDirPath = os.path.join(StudSeqDir, TempWithRamFileDir)
        if not os.path.exists(tempWithRamStudSeqDirPath):
            os.makedirs(tempWithRamStudSeqDirPath)

    # Access statement file and create iterator
    statementfile = open(statementsPath, "rb")
    studentSequencefile = line_count(statementsPath)
    print(studentSequencefile, "statements")
    
    # Init Data description we want to exploit during parsing
    dataDescription = genDataDescription()

    # Init Student Ids File
    studentIdsFilePath = os.path.join(StudSeqDir, CompiledStudSeqDir, StudentIdListFileName)
    with open(studentIdsFilePath, "w") as studentIdsFile:
        studentIdsFile.write("")
    
    # Init list of student Id
    listStudentIdSeen = []

    #Ram not friendly
    if(not no_Ram):
        allStudentData = {}

    if (filePerLine):
        iterator = statementfile
        iteratorIsJson = False
    else:
        iterator = ijson.items(statementfile, 'item')
        iteratorIsJson = True

    with tqdm.tqdm(total=os.path.getsize(statementsPath)) as pbar:
        # going through all statements
        for entry in iterator:
            if (not iteratorIsJson):
                fullStatement = json.loads(entry)
            else:
                fullStatement = entry
            statement = fullStatement["statement"]

            # get student Id
            studId = statement["actor"]["account"]["name"]
            
            if studId not in listStudentIdSeen:
                
                if(not no_Ram):
                    allStudentData[studId] = {
                        "studentdId": studId
                    }

                for typeData in TypeOfSeqData:
                    if(no_Ram):
                        studFileName = f"{studId}_{typeData}.json"
                        studFilePath = os.path.join(noRamAllStudFilesPath, studFileName)
                        with open(studFilePath, "w") as studFile:
                            studFile.write("")
                    else:
                        allStudentData[studId][typeData] = []
                                        
                with open(studentIdsFilePath, "a") as studentIdsFile:
                    studentIdsFile.write(studId)
                    studentIdsFile.write("\n")
            
                listStudentIdSeen.append(studId)

            if (checkIfStatementIsOfAnExercise(statement)):  # if the statement is about an exercise
                # get exercise infos
                infoToAdd = getExerciseInfo(statement, dataDescription)

            else:  # else it is a tutorial video statement
                infoToAdd = {"tutorialVideo": getVideoInfos(statement)}
            
            for seqType, info in infoToAdd.items():
                if(no_Ram):
                    studFileName = f"{studId}_{seqType}.json"
                    studFilePath = os.path.join(noRamAllStudFilesPath, studFileName)
                    with open(studFilePath, "a") as studFile:
                        studFile.write(json.dumps(info))
                        studFile.write("\n")
                else:
                    allStudentData[studId][seqType].append(info)

            pbar.update(len(entry))

        if(not no_Ram):
            tempWithRamStudSeqFilePath = os.path.join(tempWithRamStudSeqDirPath, TempAllStudSeqFileName)
            with open(tempWithRamStudSeqFilePath, "w") as tempAllStudSeqFile:
                tempAllStudSeqFile.write(json.dumps(allStudentData))
            del allStudentData

        statementfile.close()


def getExerciseInfo(statement, dataDescription):
    """ Get the exercise informations from a statement 

    Parameters
    ----------
    statement : dict
        the json data of the statement

    dataDescription: dict
        entries with the revelant descriptions element for all items (moudles, objectives, activities, exercises)

    Returns
    -------
    dict
        the exercise infos as a dict
    """

    exerciseInfos = {}
    # get timestamp for ordering exercices sequence
    exerciseInfos["date"] = getDateInfosFromStatement(statement)

    # get module objective and activity ids

    # get if exercise is an intial test erxercise or not, it is important because all student does the same initial test, so there is no personalization for initial test activities
    isiIitialTest = find_value_for_key_in_dict(statement, "initialTest")
    moaInfosSplitted = statement["context"]["contextActivities"]["parent"][0]["id"].split(
        "/")
    exerciseInfos["moduleId"] = moaInfosSplitted[moaInfosSplitted.index("module") + 1]
    exerciseInfos["objectiveId"] = moaInfosSplitted[moaInfosSplitted.index("objective") + 1]
    exerciseInfos["activityId"] = moaInfosSplitted[moaInfosSplitted.index("activity") + 1]

    # get exercise id
    eInfosSplitted = statement["object"]["id"].split("/")
    exerciseInfos["exerciseId"] = eInfosSplitted[eInfosSplitted.index("exercise") + 1]
    try:
        exerciseInfos["exerciseIdx"] = dataDescription["exercises"][exerciseInfos["exerciseId"]]["index"]
    except:
        #print(f"Exercise {exerciseInfos["exerciseId"]} as been renamed and is not more in the data description")
        exerciseInfos["exerciseIdx"] = -1

    # get infos we want from exercices, modify if needed
    exerciseInfos["score"] = {}
    wantedInfos = ["raw", "scaled","duration"]  #, "success", response, duration]
    for infoKey in wantedInfos:
        exerciseInfos["score"][infoKey] = find_value_for_key_in_dict(statement["result"], infoKey)

    if (isiIitialTest):
        exerciseInfos["codeActivity"] = f"{MapCodeIdAD[exerciseInfos["moduleId"]]}O0"
        return {"initialTest": exerciseInfos}
    else:
        exerciseInfos["codeActivity"] = MapCodeIdAD[exerciseInfos["activityId"]]
        simplifiedInfos = {
            "act": exerciseInfos["codeActivity"], 
            "res": round(exerciseInfos["score"]["scaled"],2), 
            "exIdx": exerciseInfos["exerciseIdx"], 
            "date": exerciseInfos["date"]
        }
        return {"adaptivCuriculum": exerciseInfos, "simpleAdaptivCuriculum": simplifiedInfos}


def getVideoInfos(statement):
    """ Get the turotial video information from a statement 

    Parameters
    ----------
    statement : dict
        the json data of the statement


    Returns
    -------
    dict
        the tutorial video infos as a dict
    """

    videoInfos = {}
    # get timestamp for ordering exercices sequence
    videoInfos["date"] = getDateInfosFromStatement(statement)

    # get other infos
    videoInfos["idRelatedActivity"] = find_value_for_key_in_dict(
        statement["context"], "id")
    ressourcesInfosSplitted = statement["object"]["id"].split("/")
    videoInfos["ressourcesInfos"] = ressourcesInfosSplitted[ressourcesInfosSplitted.index("resources")+1].split("?")[0]

    return videoInfos

def genDataDescription(rawData=RawDataDescription):
    """ Generate Data Description we wana want to use during the parsing from the Raw Data Description File 

    Parameters
    ----------
    rawData : dict
        the raw Data Description


    Returns
    -------
    dict
        the organized data
    """

    # Intit data frame
    dataDescription = {}

    hierarchyOfItemsTypes = ["modules", "objectives", "activities", "exercises"]

    # Go through items in raw data
    for itemType in hierarchyOfItemsTypes:
        dataDescription[itemType] = {}
        for hitems in rawData[itemType]:
            if(itemType == "exercises"): 
                dataDescription[itemType][hitems["id"]] = {
                    "index": hitems["index"],
                    "instruction": hitems["instruction"]["$html"]
                }
            else: 

                dataDescription[itemType][hitems["id"]] = {
                    "shortTitle": hitems["title"]["short"],
                    "longTitle": hitems["title"]["long"],
                    "description": hitems["descriptions"]
                }


    return dataDescription

def getDateInfosFromStatement(statement):
    """ Get the date information from a statement 

    Parameters
    ----------
    statement : dict
        the json data of the statement


    Returns
    -------
    str
        the string timestamp
    """

    # get timestamp for ordering exercices sequence
    rawDateInfos = statement["timestamp"]

    # format of date can change between statement due to historical change in our data pipeline
    if isinstance(rawDateInfos, dict) and "$date" in rawDateInfos:
        dateInfos = rawDateInfos["$date"]
    else:
        dateInfos = rawDateInfos
    return dateInfos


def checkIfStatementIsOfAnExercise(statement):
    """ Check if a statement is one of an exercice or a tutorial video

    Parameters
    ----------
    statement : dict
        the json data of the statement


    Returns
    -------
    int
        return 1 if it is the statement of an exercise, 0 else
    """

    if (statement["verb"]["display"]["en-US"] == "passed" or statement["verb"]["display"]["en-US"] == "failed"):
        return 1
    elif (statement["verb"]["display"]["en-US"] == "viewed"):
        return 0
    else:
        print(statement)
        print(statement["verb"])

    return


""" Due to the structure of the statements, it can be tricky to access some infos,
#   because some key may change at some point so we need to find the key
#   and value through searching the statement structure  """


def find_value_for_key_in_dict(dictInput, key_to_find):
    """ Find a value for a particular key in the data structure

    Parameters
    ----------
    dictInput : dict
        the data dictionarry to find the value in
    key_to_find : string
       The key for which to find the value

    Returns
    -------
    str or None
        the found value, None if not found
    """

    if key_to_find in dictInput:
        return dictInput[key_to_find]

    for key, value in dictInput.items():
        if isinstance(value, dict):
            result = find_value_for_key_in_dict(value, key_to_find)
            if result is not None:
                return result
        elif isinstance(value, list):
            for item in value:
                if isinstance(item, dict):
                    result = find_value_for_key_in_dict(item, key_to_find)
                    if result is not None:
                        return result
    return None


def find_key_for_value_in_dict(dictInput, value_to_find):
    """ Find a key for a particular value in the data structure

    Parameters
    ----------
    dictInput : dict
        the data dictionarry to find the key in
    value_to_find : string
       The value for which to find the key

    Returns
    -------
    str or None
        the found key, None if not found
    """

    if value_to_find in dictInput.values():
        key_list = list(dictInput.keys())
        val_list = list(dictInput.values())
        position = val_list.index(100)

        return key_list[position]

    for key, value in dictInput.items():
        if isinstance(value, dict):
            result = find_key_for_value_in_dict(value, value_to_find)
            if result is not None:
                return result
        elif isinstance(value, list):
            for item in value:
                if isinstance(item, dict):
                    result = find_key_for_value_in_dict(item, value_to_find)
                    if result is not None:
                        return result
    return None


def line_count(file_path):
    """ Count number of lines in file

    Parameters
    ----------
    path : str
        the path of the file


    Returns
    -------
    int
        number of lines in the file
    """

    return int(os.popen(f'wc -l {file_path}').read().split()[0])


def get_dir_size(path='.'):
    """ Get the binary size of a directory 

    Parameters
    ----------
    path : str
        the path of the directory


    Returns
    -------
    int
        size of the directory
    """

    total = 0
    with os.scandir(path) as it:
        for entry in it:
            if entry.is_file():
                total += entry.stat().st_size
            elif entry.is_dir():
                total += get_dir_size(entry.path)
    return total


if __name__ == '__main__':
    main()
