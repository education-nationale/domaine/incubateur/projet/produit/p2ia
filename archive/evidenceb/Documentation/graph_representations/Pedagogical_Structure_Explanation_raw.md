### Organisation de l'espace des activity pédagogiques et personnalisation

Adaptiv'Math est organisé en 7 Modules. Un modules représente un graph d'activité cohérent et fermé, il n'y a pas d'interaction entre les modules. Ainsi, le test initial et la personnalisation du curriculum se fait à l'intérieur d'un module sans impact sur le test intial et la personnalisation des autre modules. 

Il y a un module commun à tout les élève, puis des modules dédiés pour le CP, le CE1 et le CE2. 
Le module commun est le module 1, puis pour les module 3x et 4x, x représente pour quelle classe est dédiée le module: 
* 31 et 41 pour le CP
* 32 et 42 pour le CE1
* 33 et 43 pour le CE2

Un module se compose d'objectifs, chaque objectif correspond une connaissance particulière. Un objectif se compose d'activités, ces activités correspondent à des composantes de connaissance de l'objectif et sont organisé un peu comme en niveau de difficultés au sein d'un objectif. Pour chaque activité, il y a un ensemble d'exercises <u>équivalents</u> qui permettent de travailler cette activité. 

Des représentations des graphs de ces différents modules sont présente dans le dossier [représentation des graphs](graph_representations/) 

```python
from wand.image import Image as WImage
```

```python
# choose module Index
moduleIdx = 42
```

Il y a des représentation éclaté des modules

```python
WImage(filename=f'graph_representations/graphAM_M{moduleIdx}.pdf')
```

Et il y a des représentatiosn à plat

```python
WImage(filename=f'graph_representations/graphAM_M{moduleIdx}_flat.pdf')
```