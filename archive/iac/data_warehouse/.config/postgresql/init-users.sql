CREATE OR REPLACE FUNCTION create_user_and_grant_privileges(username TEXT) RETURNS VOID AS $$
BEGIN
    -- Tente de créer l'utilisateur seulement s'il n'existe pas déjà
    IF NOT EXISTS (SELECT FROM pg_catalog.pg_roles WHERE rolname = username) THEN
        EXECUTE format('CREATE USER %I WITH PASSWORD %L', username, username);
    END IF;
    
    -- Grant privileges
    EXECUTE format('GRANT CONNECT ON DATABASE entrepot_de_donnees TO %I', username);
    EXECUTE format('GRANT USAGE ON SCHEMA public TO %I', username);
    EXECUTE format('GRANT SELECT ON ALL TABLES IN SCHEMA public TO %I', username);
    EXECUTE format('ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA public GRANT SELECT ON TABLES TO %I', username);
END;
$$ LANGUAGE plpgsql;

-- Appel de la fonction pour un utilisateur
SELECT create_user_and_grant_privileges('atellech');
SELECT create_user_and_grant_privileges('ccimelli3');
SELECT create_user_and_grant_privileges('jmframery');
SELECT create_user_and_grant_privileges('mdacruz');
SELECT create_user_and_grant_privileges('nassimyazi');
