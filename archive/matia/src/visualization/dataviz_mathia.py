"""Visualisation des données brutes de matia"""

import os
import time
import yaml
from src.visualization import dashboard
from src.features import convert_files, sub_dict_json, action_on_folder
import pandas as pd
import json
start = time.time()  # Heure de début de script

# with open("config\config.yaml\path.yaml") as f:
#         path = yaml.safe_load(f)

##############################USAGE_APP##################################


# # Conversion des csv en json

# csv_folder = action_on_folder.create_folder("matia/data/interim", "csv_to_json")
# USAGE_DATA = "matia/data/raw/usage_app"

# for file in os.listdir(USAGE_DATA):
#     # print(file)
#     path_file = os.path.join(USAGE_DATA, file)
#     # print(path_file)
#     convert_files.convert_csv_to_json(path_file, csv_folder)

# # Récupération des sous-dicionnaires

# sub_dict_folder = action_on_folder.create_folder(
#     "matia/data/interim", "json_with_sub_dict"
# )
# for file in os.listdir("matia/data/interim/csv_to_json"):
#     # print(file)
#     path_file = os.path.join("matia/data/interim/csv_to_json", file)
#     sub_dict_json.get_sub_dict_from_json(path_file, sub_dict_folder)

# # Conversion des nouveaux json en CSV

# csv_folder = action_on_folder.create_folder("matia/data/interim", "csv_with_sub_dict")
# for file in os.listdir("matia/data/interim/json_with_sub_dict"):
#     # print(file)
#     path_file = os.path.join("matia/data/interim/json_with_sub_dict", file)
#     convert_files.convert_json_to_csv(path_file, csv_folder)

# # Visualisation des données

# for file in os.listdir("matia/data/interim/csv_with_sub_dict"):
#     # print(file)
#     path_file = os.path.join("matia/data/interim/csv_with_sub_dict", file)
#     # print(path_file)
#     dashboard.analysis(
#         path_file, "matia/reports", "matia/reports/sub_dict", folder_name="sub_dict"
#     )
##################################################################################

###########################TRACES_APPRENTISSAGE####################################


# TRACES_DATA = "matia/data/raw/traces_apprentissage"

# simplified_json = action_on_folder.create_folder(
#     "matia/data/interim", "simplified_json"
# )
# nombre_de_lignes = 100
# for file in os.listdir(TRACES_DATA):
    #path_file = os.path.join(TRACES_DATA, file)


##AFFICHAGE DU JSON EN LIGNE
#     with open(path_file, 'r') as fichier:
#         print(file)
#         for i in range(nombre_de_lignes):
#             ligne = fichier.readline()
#             print(ligne)

##TRANSFORMATION DES LIGNES EN OBJETS JSON
#files = ["lrs_2022-2023.json", "lrs_2023-2024.json"]
#for file in files:
    
    # try:
    #     input_file_path = os.path.join(TRACES_DATA, file)
    #     output_file_path = os.path.join(simplified_json, f'transformed_{file}')

    #     # Lire le fichier JSON complet
    #     with open(input_file_path, 'r', encoding='utf-8') as infile:
    #         json_content = infile.read()
        
    # # Reconstituer les objets JSON
    #     try:
    #         json_objects = json.loads(json_content)
    #         if not isinstance(json_objects, list):
    #             json_objects = [json_objects]
    #     except json.JSONDecodeError as e:
    #         print(f"Error decoding JSON in file {file}: {e}")
    #         continue
        
    #     # Écrire chaque objet JSON en une seule ligne dans le fichier de sortie
    #     with open(output_file_path, 'w', encoding='utf-8') as outfile:
    #         for obj in json_objects:
    #             json.dump(obj, outfile)
    #             outfile.write('\n')
        
    #     print(f"Finished transforming file: {file}")

    # except Exception as e:
    #     print(f"An unexpected error occurred while processing file {file}: {e}") 
    
# def split_json(file_path):
#     with open(file_path, 'r') as json_file:
#         data = json.load(json_file)

#         chunk_size = len(data) // 3
#         for i in range(3):
#             with open(f"part{i}.json", 'w') as outfile:
#                 outfile.write(json.dumps(data[i*chunk_size:(i+1)*chunk_size]))

#         file_path = input("Enter the file path of the JSON file: ")
#         split_json(file_path)   

# # Chunks JSON

# chunk_size = 200000
# x=2021
# y=2022

# for file in os.listdir("matia/data/interim/simplified_json"):    

# # Parcourir tous les fichiers dans le dossier input_folder
#     x+=1
#     y+=1
#     chunks_folder = action_on_folder.create_folder(
#     "matia/data/interim", f"chunks_{x}-{y}"
# )
#     json_folder = action_on_folder.create_folder(
#     "matia/data/interim", f"jsonl_to_json_{x}-{y}"
# )

#     path_file = path_file = os.path.join("matia/data/interim/simplified_json", file)
    
#     with open(path_file, 'r') as f:
#         lines = []
#         chunk_index = 0  # Initialiser l'index du chunk
#         print(file)
#         for line_index, line in enumerate(f):
#             try:
#             #print(line_index, line)
#             #print(line_index)
#             # if line_index == 1595493:
#             #     print(line)
            
#                 lines.append(json.loads(line))
#             except json.JSONDecodeError as e: 
#                 print(f"Error decoding JSON on line {line_index}: {e}")
#                 continue  # Ignorer la ligne mal formée et continuer                
#             # Si la taille du chunk atteint chunk_size, sauvegarder et réinitialiser
#             if (line_index + 1) % chunk_size == 0:
#                 chunk_df = pd.DataFrame(lines)
#                 chunk_path = os.path.join(chunks_folder, f'{x}-{y}_chunk_{chunk_index}.json')
#                 chunk_df.to_json(chunk_path, orient='records', lines=True)
#                 convert_files.convert_jsonl_to_json(chunk_path, json_folder)
#                 chunk_index += 1
#                 lines = []
#         # Sauvegarder le dernier chunk s'il en reste
#         if lines:
#             chunk_df = pd.DataFrame(lines)
#             chunk_path = os.path.join(chunks_folder, f'{x}-{y}_chunk_{chunk_index}.json')
#             chunk_df.to_json(chunk_path, orient='records', lines=True)
#             convert_files.convert_jsonl_to_json(chunk_path, json_folder)

# # Récupération des sous-dictionnaires

# sub_dict_folder_2022 = action_on_folder.create_folder(
# "matia/data/interim", f"chunks_with_simple_sub_dict_2022-2023"
# )
# sub_dict_folder_2023 = action_on_folder.create_folder(
# "matia/data/interim", f"chunks_with_simple_sub_dict_2023-2024"
# )

# for file in os.listdir("matia/data/interim/jsonl_to_json_2022-2023"):
#     # print(file)
#     path_file = os.path.join("matia/data/interim/jsonl_to_json_2022-2023", file)
#     sub_dict_json.get_sub_dict_from_json(path_file, sub_dict_folder_2022)


# for file in os.listdir("matia/data/interim/jsonl_to_json_2023-2024"):
#     # print(file)
#     path_file = os.path.join("matia/data/interim/jsonl_to_json_2023-2024", file)
#     sub_dict_json.get_sub_dict_from_json(path_file, sub_dict_folder_2023)


# # Conversion des nouveaux json en CSV

# csv_folder_2022 = action_on_folder.create_folder(
# "matia/data/interim", f"chunks_csv_2022-2023"
# )
# csv_folder_2023 = action_on_folder.create_folder(
# "matia/data/interim", f"chunks_csv_2023-2024"
# )

# for file in os.listdir("matia/data/interim/chunks_with_simple_sub_dict_2022-2023"):
#     # print(file)
#     path_file = os.path.join("matia/data/interim/chunks_with_simple_sub_dict_2022-2023"
#                              , file)
#     convert_files.convert_json_to_csv(path_file, csv_folder_2022)


# for file in os.listdir("matia/data/interim/chunks_with_simple_sub_dict_2023-2024"):
#     # print(file)
#     path_file = os.path.join("matia/data/interim/chunks_with_simple_sub_dict_2023-2024", file)
#     convert_files.convert_json_to_csv(path_file, csv_folder_2023)

# Fusion des chunks csv

# csv_folder = action_on_folder.create_folder("matia/data/interim", "csv_with_sub_dict")

# csv_2022=[]

# for file in os.listdir("matia/data/interim/chunks_csv_2022-2023"):
#     path_file = os.path.join("matia/data/interim/chunks_csv_2022-2023", file)
#     csv_2022.append(pd.read_csv(path_file))

# treated_lrs_2022 = pd.concat(csv_2022, ignore_index=True)

# treated_lrs_2022.to_csv("matia/data/interim/csv_with_sub_dict/treated_lrs_2022-2023.csv", index=False)

# csv_2023=[]

# for file in os.listdir("matia/data/interim/chunks_csv_2023-2024"):
#     path_file = os.path.join("matia/data/interim/chunks_csv_2023-2024", file)
#     csv_2023.append(pd.read_csv(path_file))

# treated_lrs_2023 = pd.concat(csv_2023, ignore_index=True)

# treated_lrs_2023.to_csv("matia/data/interim/csv_with_sub_dict/treated_lrs_2023-2024.csv", index=False)

# # Visualisation des données

for file in os.listdir("matia/data/interim/csv_with_sub_dict"):
    # print(file)
    path_file = os.path.join("matia/data/interim/csv_with_sub_dict", file)
    # print(path_file)
    dashboard.analysis(
        path_file, "matia/reports", "matia/reports/sub_dict", folder_name="sub_dict"
    )

# print(time.time())
# print(start)
# print(time.ctime()[11:19])
# print((time.ctime(start))[11:19])
print(
    time.strftime("%H:%M:%S", time.gmtime(time.time() - start))
)  # Temps d'exexcution du script
