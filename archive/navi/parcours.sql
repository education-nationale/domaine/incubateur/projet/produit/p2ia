SELECT
    '{nom_outil}' as nom_outil,
    student_id as id_elev,
    GROUP_CONCAT(DISTINCT content_id) AS parcours,
    COUNT(DISTINCT content_id) AS nb_activites,
    COUNT(content_id) AS nb_activites_dup,
    COUNT(DISTINCT knowledge_node_id) AS nb_competences,
    COUNT(
        DISTINCT
        CASE
            WHEN success = 1 THEN knowledge_node_id
            ELSE NULL
        END
    ) AS nb_comp_acquises,
    GROUP_CONCAT(DISTINCT knowledge_node_id) AS lst_competences,
    CASE
        WHEN STRFTIME('%d-%m', created_at) >= {school_date_start}
            THEN STRFTIME('%Y', created_at) || '-' || STRFTIME('%Y', created_at, '+1 year')
        ELSE STRFTIME('%Y', created_at, '-1 year') || '-' || STRFTIME('%Y', created_at)
    END AS annee_scolaire,
    MIN(created_at) AS dt_debut,
    MAX(updated_at) AS dt_fin,
    NULL AS nb_verbes
FROM
    {table}
GROUP BY
    id_elev,
    annee_scolaire;
