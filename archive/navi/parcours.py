import pandas as pd
import pandasql as ps
from navi.methods import get_data
from constants import school_date_start


def db(query: str) -> pd.DataFrame:
    """
    Execute a SQL query on a DataFrame

    :param query: SQL query
    :return: DataFrame
    """
    return ps.sqldf(query, globals())


df = get_data('./navi/data_sources/events.csv')
df['created_at'] = pd.to_datetime(
    df['created_at'], format='%Y-%m-%d %H:%M:%S.%f')
df['updated_at'] = pd.to_datetime(
    df['updated_at'], format='mixed')

df['annee'] = df['created_at'].apply(lambda x: f"{x.year}-{(x.year+1)%100}")

df_sql = db(
    query=open('./navi/parcours.sql').read().format(
        table='df',
        nom_outil='navi',
        school_date_start=school_date_start,
    )
)

df_sql.to_csv('bdd_commune/tables/parcours.csv', index=False)
