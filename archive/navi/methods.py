import os
import csv
import pandas as pd
from datetime import datetime
import matplotlib.pyplot as plt


def count(col: str, src: str, start_at: datetime, end_at: datetime,
          delim: str = None, is_active: bool = False,
          format_created_at: str = '%Y-%m-%d %H:%M:%S') -> int:
    """
    Count the number of unique values in a column

    :param col: column name
    :param src: source of the data
    :param start_at: start date
    :param end_at: end date
    :param delim: delimiter of the data
    :param is_active: if the user is active
    :param format_created_at: format of the created_at column
    :return: number of unique values
    """
    data = get_data(src, delim)
    if is_active:
        data = data[(data['deleted_at'].isna()) & (data['last_login'].notna())]
    try:
        data['created_at'] = pd.to_datetime(
            data['created_at'], format=format_created_at
        )
        data = data[
            (data['created_at'] >= start_at) & (data['created_at'] <= end_at)
        ]
    except KeyError:
        pass
    return data[col].nunique()


def get_data(src: str, delim: str = None) -> pd.DataFrame:
    """
    Get the data from a source

    :param src: source of the data
    :param delim: delimiter of the data
    :return: DataFrame
    """
    return pd.read_csv(src, delimiter=delim, low_memory=False)


def dict_to_csv(dicts: dict, output: str) -> None:
    """
    Save a dictionary to a CSV file

    :param dicts: dictionary
    :param output: output file
    """
    df = pd.DataFrame(dicts)
    df.to_csv(output, index=False)


def merge_csv(csv_path: str, data: dict) -> None:
    """
    Merge a CSV file with a dictionary

    :param csv_path: path to the CSV file
    :param data: dictionary
    """
    df = pd.read_csv(csv_path)
    df_data = pd.DataFrame(data)
    print(df_data)
    df = pd.concat([df, df_data], ignore_index=True)
    df.drop_duplicates(
        subset=['nom_outil', 'annee'], keep='last', inplace=True
    )
    df.to_csv(csv_path, index=False)


def avg(col: str, df: pd.DataFrame) -> float:
    """
    Get the average of a column

    :param col: column name
    :param df: DataFrame
    :return: average of the column
    """
    return df.groupby(col).size().mean().round(0)


def positive_student_rate(col: str, df: pd.DataFrame) -> float:
    """
    Get the positive rate of a column grouped by student

    :param col: column name
    :param df: DataFrame
    :return: positive rate of the column
    """
    return (df[df[col] == True].groupby('student_uid')[col].count() /
            df.groupby('student_uid')[col].count()).mean().round(2) * 100


def analyze_events() -> tuple:
    """
    Analyze the events data

    :return: tuple
    """
    data = get_data('./navi/data_sources/events.csv')
    success_rate = positive_student_rate(col='success', df=data)
    avg_events = avg(col='student_uid', df=data)
    most_active_student = data.groupby('student_uid').size().idxmax()

    return success_rate, avg_events, most_active_student


def calculate_validation_rate_obj_stdt_assmt() -> float:
    """
    Calculate the validation rate from the objective student assessment data

    :return: validation rate
    """
    data = get_data('./navi/data_sources/objective_student_assessments.csv')
    validation_rate = positive_student_rate(col='validated', df=data)

    return validation_rate


def kns(student_id: int, kn_id: int) -> pd.DataFrame:
    """
    Traces the learning of the students

    :param student_id: student unique identifier
    :param kn_id: knowledge node identifier
    :return: boolean indicating if the operation was successful
    """
    root = 'navi/data_sources/knowledge_node_students'
    dfs = []
    for filename in os.listdir(root):
        if filename.endswith('.csv'):
            file_path = os.path.join(root, filename)
            df = pd.read_csv(file_path)
            df_filtered = df[
                (df['student_id'] == student_id)
                & pd.notna(df['memory_level'])
                & (df['knowledge_node_id'] == kn_id)
            ]
            dfs.append(df_filtered)

    data = pd.concat(dfs, ignore_index=True)

    return data[[
        'student_id', 'knowledge_node_id', 'memory_level', 'memory_strength',
        'last_delta_review'
    ]].sort_values(by='memory_level', ascending=False).drop_duplicates(
        subset='knowledge_node_id')


def events(student_id: int, kn_id: int) -> pd.DataFrame:
    """
    Get the events of a student

    :param student_id: student unique identifier
    :param kn_id: knowledge node identifier
    :return: DataFrame
    """
    data = get_data('./navi/data_sources/events.csv')
    data = data[
        (data['student_id'] == student_id)
        & (data['knowledge_node_id'] == kn_id)
    ]

    return data[[
        'student_id', 'knowledge_node_id', 'success', 'created_at'
    ]].sort_values(by='created_at')


def events_most_active() -> dict:
    """
    Get the knowledge node ID that appears most for the specific student ID

    :return: dict
    """
    data = get_data('./navi/data_sources/events.csv')

    grouped_data = data.groupby(
        ['student_id', 'knowledge_node_id']
    ).size().reset_index(name='counts')

    stdt_id = grouped_data['student_id'].value_counts().idxmax()
    specific_student_data = data[data['student_id'] == stdt_id]
    grouped_data = specific_student_data.groupby('knowledge_node_id').size()
    knowledge_node_id = grouped_data.idxmax()

    return {
        'student_id': stdt_id,
        'knowledge_node_id': knowledge_node_id
    }


def graph_events() -> None:
    """
    Graph the events data
    """
    stud_id, know_id = events_most_active()['student_id'], events_most_active()[
        'knowledge_node_id']

    df_events = events(student_id=stud_id, kn_id=know_id)

    df_events['created_at'] = pd.to_datetime(df_events['created_at'])
    df_events = df_events.sort_values('created_at')

    df_events['date_diff'] = df_events['created_at'].diff().dt.seconds / 60

    df_events = df_events.dropna(subset=['date_diff']).sort_values('created_at')
    print(df_events)
    plt.figure(figsize=(10, 6))
    plt.plot(df_events['created_at'].values, df_events['date_diff'].values)
    plt.show()


def count_class_active(start_at: datetime, end_at: datetime):
    df = get_data('./navi/data_sources/events.csv')
    df_class = get_data('./navi/data_sources/students_class.csv', ';')
    df['created_at'] = pd.to_datetime(
        df['created_at'], format='%Y-%m-%d %H:%M:%S.%f')
    df = df[
        (df['created_at'] >= start_at) & (df['created_at'] <= end_at)
    ]
    df.groupby('student_uid')
    df_join = pd.merge(
        df, df_class, how='inner', left_on='student_uid',
        right_on='student_uid').drop_duplicates(subset='class_uid')
    return df_join['class_uid'].nunique()
