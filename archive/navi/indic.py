from datetime import datetime
from constants import school_date_start, school_date_end
from navi.methods import count, count_class_active, merge_csv

annee_scolaire = []
nb_prof = []
nb_prof_act = []
nb_eleve = []
nb_eleve_act = []
nb_class = []
nb_class_act = []

for year in range(2020, datetime.now().year):
    start_at = datetime.strptime(f"{school_date_start}-{year}", "%d-%m-%Y")
    end_at = datetime.strptime(f"{school_date_end}-{year+1}", "%d-%m-%Y")
    count_teacher = count(
        col='id',
        src='./navi/data_sources/teachers.csv',
        delim=';',
        format_created_at='%m/%d/%y %H:%M',
        start_at=start_at,
        end_at=end_at,
    )
    count_teacher_active = count(
        col='id',
        src='./navi/data_sources/teachers.csv',
        delim=';',
        is_active=True,
        format_created_at='%m/%d/%y %H:%M',
        start_at=start_at,
        end_at=datetime.strptime(f"{school_date_end}-{year + 1}", "%d-%m-%Y"),
    )
    count_student = count(
        col='student_uid',
        src='./navi/data_sources/students.csv',
        format_created_at='%Y-%m-%d %H:%M:%S.%f',
        start_at=start_at,
        end_at=end_at,
    )
    count_student_active = count(
        col='student_uid',
        src='./navi/data_sources/events.csv',
        format_created_at='%Y-%m-%d %H:%M:%S.%f',
        start_at=start_at,
        end_at=datetime.strptime(f"{school_date_end}-{year + 1}", "%d-%m-%Y"),
    )
    count_class = count(
        col='class_uid',
        src='./navi/data_sources/students_class.csv',
        delim=';',
        start_at=start_at,
        end_at=end_at,
    )
    cnt_class_active = count_class_active(
        start_at=start_at,
        end_at=end_at,
    )
    if count_student > 0 and count_teacher > 0:
        annee_scolaire.append(f"{year}-{(year+1)%100}")
        nb_prof.append(count_teacher)
        nb_prof_act.append(count_teacher_active)
        nb_eleve.append(count_student)
        nb_eleve_act.append(count_student_active)
        nb_class.append(count_class)
        nb_class_act.append(cnt_class_active)

dicts = {
    "nom_outil": "navi", "annee": annee_scolaire,
    "nb_profs_inscrits": nb_prof, "nb_profs_util": nb_prof_act,
    "nb_eleves_inscrits": nb_eleve, "nb_eleves_util": nb_eleve_act,
    "nb_classe_inscrits": nb_class, "nb_classe_util": nb_class_act,
    "nb_établissement_util": None, "nb_établissement_inscrits": None
}
print(dicts)
merge_csv(
    data=dicts,
    csv_path='bdd_commune/tables/indic_generaux.csv',
)
