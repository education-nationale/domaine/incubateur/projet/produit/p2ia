import logging
from typing import Literal
from load import load_to_postgres
from engine import DatabaseEngine

logging.basicConfig(level=logging.INFO)


def data_to_pstgr_master(
        data, db_name: Literal['parcours', 'indic_generaux'],
        need_transform: bool = False, transform_file: str = None,
        path: str = 'bdd_commune/script/data/engineering',
        ) -> bool:
    """
    Load data to the database.

    Parameters:
    data (str or DataFrame): Data to be loaded. If it's a string,
                             it's assumed to be the name of the dataset.
    path (str): Path to the raw data file (if applicable).
    db_name (str): Name of the database.

    Returns:
    bool: True if data loaded successfully, False otherwise.
    """
    db = DatabaseEngine()

    try:
        if not isinstance(data, str):
            if load_to_postgres(
                    data=data, env='staging', db_name=db_name, path=path):
                logging.info('Data loaded successfully to staging')

                # Staging to master
                with open(f'{path}/master/{db_name}.sql') as file:
                    query = file.read().format(
                        env_cible='master', env_source='staging'
                    )
                db.run_query(query)

                logging.info('Data loaded successfully to master')
                return True
            else:
                logging.error('Data not loaded to staging')
                return False
        else:
            if load_to_postgres(
                    data=data, env='raw', db_name=db_name, path=path):
                logging.info('Data loaded successfully to raw')

                if need_transform:
                    # Raw to staging
                    with open(
                            f'{path}/staging/transform/{transform_file}.sql'
                    ) as file:
                        query = file.read()
                    db.run_query(query)
                    logging.info('Data loaded successfully to staging')

                    # Staging to master
                    with open(f'{path}/master/{db_name}.sql') as file:
                        query = file.read().format(
                            env_cible='master', env_source='staging',
                            table_name=db_name
                        )

                else:
                    # Create master table
                    with open(f'{path}/master/create.sql') as file:
                        query = file.read().format(
                            source=db_name,
                            env_source='raw'
                        )
                    db.run_query(query)

                    # Raw to master
                    with open(f'{path}/master/{db_name}.sql') as file:
                        query = file.read().format(
                            env_cible='master', env_source='raw',
                            table_name=db_name
                        )

                db.run_query(query)

                logging.info('Data loaded successfully to master')
                return True
            else:
                logging.error('Data not loaded to raw')
                return False
    except Exception as e:
        logging.exception(
            'An error occurred during the data loading process',
            exc_info=e
        )
        return False


if __name__ == '__main__':
    data_to_pstgr_master(
        data='indic_generaux_evidenceb.csv', db_name='indic_generaux',
    )
