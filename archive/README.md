# Analyse des données des outils P2IA

## Aperçu
Dans le cadre du projet P2IA, nous avons effectué une exploration approfondie 
des données de la base de données des outils P2IA. Cette base de données 
comprend les entreprises suivantes :
- Navi
- MathIA
- SmartEnseigno

Nous avons effectué une comparaison à un niveau macro de tous les outils en 
utilisant divers indicateurs globaux, tels que :

- **Le nombre d'utilisateurs actifs**, en tenant compte de divers facteurs 
  tels que : 
  - la temporalité: daily, weekly, monthly
  - le type utilisateur : élève et professeur
- **Le taux de réussite moyen par élève** sur les différentes évaluations
- **Le taux de tâches ou d'activités complétées par élève**
- **Le taux d'abandon** sur les différentes activités
- **Le temps moyen passé par élève sur l'outil**

### Recommendations
> _Veuillez respecter les nomenclatures suivantes pour la variabilisation des 
> indicateurs_ :
> - `count_teacher_active`
> - `count_student_active_{daily}`
> - `rate_success_student`
> - `rate_task_completed_student`
> - `rate_abandon_student`
> - `time_spent_student`


# Packages réutilisables

Afin d'éviter de devoir recopier un script ou d'importants bouts de code à plusieurs reprise, on a créé un package que l'on peux ensuite, une fois installé, réutiliser et modifier. Le dossier **packages** contient ainsi différents modules qui peuvent être utilisés à plusieurs reprises dans les différents projets.

## Installation des packages

Afin de pouvoir installer le package, il faut tout d'abord lancer le script suivant depuis la racine du dossier **packages** :

`from setuptools import find_packages, setup`

`setup(name='src', packages=find_packages(), version='0.1.0')`

Il faut ensuite effectuer, depuis le dossier packages via le terminal, la commande `pip install -e`
Un dossier **src.egg-info** devrait alors apparaitre. Il est maintenant possible d'importer les modules du package.

## Importation au sein des scripts

Pour utiliser un module spécifique, il faut d'abord pouvoir le situer dans le dossier **packages**, lui-même composer de plusieurs sous-dossier. On importe alors dans les scripts de cette façon:

**from src.*dossier du module* import *module***

Puis, on peut lancer les fonction du module en faisant:

`module.fontion`

## Listes des outils

### Conversion des fichiers

`from src.features import convert_files`

- Conversion des JSONL en JSON
    - `convert_files.convert_jsonl_to_json(file_jsonl, json_path)`
    
    > ℹ️ JSONL: Fichier où chaque ligne correspond à un dictionnaire JSON. Chaque ligne correspond à un enregistrement
    > 
- Conversion des JSON en CSV
    - `convert_files.convert_json_to_csv(json_file, csv_path)`
    
    > 💡 Permet l’utilisation du l’outil data mining sur les fichiers JSON
    > 
- Conversion des CSV en JSON
    - `convert_files.convert_csv_to_json(csv_file, json_path)`
    
    > 💡 Permet l’utilisation du l’outil de récupération des sous dictionnaires
    > 

### Récupération des sous dictionnaire dans les JSON

`from src.features import sub_dict_json`

- Récupère les informations des sous-dictionnaires dans les JSON
- Les stocke dans un nouveau fichier JSON
    - `sub_dict_json.get_sub_dict_from_json(json_file, storage_folder)`
    
    > 🚨 Les sous-dictionnaires, qui sont en réalités des objets JSON, contiennent la majorité des informations des fichiers JSON fournis. Dans les csv, ils sont conservées dans des attributs, ce qui empêche d'analyser/traiter le fichier correctement.
    > 

### Action sur les dossiers

`from src.features import action_on_folder`

- Création de dossier
    - `action_on_folder.create_folder(path, folder_name)`

### Data mining : outil d’analyse des données

`from src.visualisation import dashboard`

- Permet de **visualiser les données** d’un **csv/xlsx** via un fichier **json, un PDF et un Dashboard**
- Permet d’obtenir les **infos de bases** sur le fichiers **csv/xlsx**
- Classifie les **attributs** selon leurs “**types**” :
    - Binaires
    - Catégoriel
    - Identifiant
    - Numérique
    - Année
    - Dates
    - Objets
- Affiche des **informations/graphiques** utiles sur selon le “type” des **attributs**
- `dashboard.analysis(file, folder_create, folder_path, sep=",", folder_name="reports")`