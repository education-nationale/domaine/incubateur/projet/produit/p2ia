import streamlit as st
import utils

st.markdown('# P2IA')
st.markdown('## Indicateurs')
st.markdown('### :blue[Utilisateurs]')
st.markdown('#### Nombre de professeurs actifs')

st.bar_chart(
    utils.teacher_active(), x='tool', y='count', color='tool'
)
