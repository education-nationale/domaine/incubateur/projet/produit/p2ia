from setuptools import find_packages, setup

setup(
    name='table',
    packages=find_packages(),
    version='0.1.0',
    description='Table packages',
)
