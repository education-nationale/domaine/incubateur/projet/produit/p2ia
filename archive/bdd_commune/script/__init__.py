import pandas as pd


indic_generaux=pd.DataFrame(columns=["nom_outil", "annee", "nb_profs_util", "nb_profs_inscrits", 
                                     "nb_eleves_util", "nb_eleves_inscrits"])
"""| Champs | Type | Commentaire |
| --- | --- | --- |
| nom_outil | caractère | Nom de l'outil |
| annee | caractère | année scolaire au format "YYYY-YY+1" exemple : 2021-22. Les années se comptent du 1er septembre au 15 juillet. |
| nb_profs_util | entier | Nombre de professeurs ayant utilisé l'outil |
| nb_profs_inscrits | entier | Nombre de professeurs inscrits |
| nb_eleves_util | entier | Nombre d'élèves ayant utilisé l'outil |
| nb_eleves_inscrits | entier | Nombre d'élèves inscrits |"""

parcours=pd.DataFrame(columns=["nom_outil", "annee", "id_elev", "parcours", "dt_debut", "dt_fin", 
                               "nb_activities","nb_verbes", "nb_activities_dup", "lst_competences", 
                               "nb_competences", "nb_comp_acquises"])
"""| Champs | Type | Commentaire |
| --- | --- | --- |
| nom_outil | caractère | Nom de l'outil |
| annee | caractère | année scolaire au format "YYYY-YY+1" exemple : 2021-22 |
| id_elev | caractère | identifiant de l'élève |
| parcours | liste | Liste Python des id d'activités; Une activité est un exercice de l'élève. La liste est séquencée et les activités sont uniques (ne pas répéter un id d'activité s'il se suit lui-même) |
| dt_debut | datetime | Date de lancement de la première activité (jj-mm-aaaa:hh:mm:ss) |
| dt_fin | datetime | Date de fin de la dernière activité (jj-mm-aaaa:hh:mm:ss) |
| nb_activites | entier | Nombre d'activités dédupliquées incluses dans le parcours |
| nb_verbes | entier | Nombre d'actions faites dans le parcours (correspond aux verbes de xAPI) |
| nb_activites_dup | entier | Nombre d'activités dans le parcours |
| lst_competences | liste | Liste des compétences étudiées dans le parcours |
| nb_competences | entier | Nombre de compétences étudiées dans le parcours |
| nb_comp_acquises | entier | Nombre de compétences acquises dans le parcours |"""


with open("bdd_commune/tables/indic_generaux.csv", "w") as csv_file:
    indic_generaux.to_csv(csv_file, sep=",", index =False)

with open("bdd_commune/tables/parcours.csv", "w") as csv_file:
    parcours.to_csv(csv_file, sep=",", index=False)