import pandas as pd
import re
import time
import ast


start = time.time()  # Heure de début de script

account= pd.read_csv("smart-enseigno/data/interim/reduce_csv/account_deep_sub_dict.csv")
assignement= pd.read_csv("smart-enseigno/data/interim/reduce_csv/assignment_complet_deep_sub_dict.csv")
organisazation=pd.read_csv("smart-enseigno/data/interim/reduce_csv/organization_deep_sub_dict.csv")



###############RECUPERATION DES ANNEES SCOLAIRES##############################
def recuperation_annee_scolaire(df, colonne_date):
    annee_complete=[] #année existante dans les données
    annee_partielle=[]
    annee_scolaire=[] #Récupération des années scolaires
    seuil_annee_scolaire=[] #Index dans le df des changement d'année scolaire

    df.sort_values(by=colonne_date, inplace=True, ignore_index=True)
    # print(df[colonne_date].head(30))
    
    for k in range (df.shape[0]) : #Mieux -> Regex
        date=df[colonne_date][k][0:10]
        #print(date)
        if int(date[0:4]) not in annee_complete and int(date[6])>=9 and int(date[9])>=1:
            annee_complete.append(int(date[0:4]))
            if k+1!=1:
                seuil_annee_scolaire.append(k+1)
    for k in range (df.shape[0]) : #Mieux -> Regex
        date=df[colonne_date][k][0:10]
        #print(date)
        if int(date[0:4]) not in annee_complete and int(date[0:4]) not in annee_partielle :
            annee_partielle.append(int(date[0:4]))
    
    seuil_annee_scolaire.append(df.shape[0])

    # print(annee_complete)
    # print(annee_partielle)
    # print(seuil_annee_scolaire)


    annee=annee_partielle+annee_complete
    annee.sort()
    #print(annee)

    for year_1 in annee:
        for year_2 in annee:
            if int(year_1)-int(year_2) == 1:
                annee_scolaire.append(f"{year_2}-{str(year_1)[2:4]}")
        #Test d'année scolaire manquante
    if len(annee_scolaire)!=len(seuil_annee_scolaire):
        nb_annee_manquante=0
        annee_precedente=annee[0]
        for k in range (1, len(annee)):
            ecart = annee[k]-annee_precedente 
            if ecart!=1:
                nb_annee_manquante+=ecart
            annee_precedente=annee[k]
        
        if nb_annee_manquante != 0:
            print(nb_annee_manquante)
        
        else: #Cas d'une année scolaire non compléte en début de liste
            annee_scolaire.insert(0,f"{annee[0]-1}-{str(annee[0])[2:4]}")

    #print(annee_scolaire)
    
    # for k in range(len(seuil_annee_scolaire)): 
    #     print(df.iloc[seuil_annee_scolaire[k]-2:seuil_annee_scolaire[k]+2, 2])

    return (annee_scolaire, seuil_annee_scolaire)

annee_scolaire=recuperation_annee_scolaire(assignement, "attributes/createdAt")[0]

#print(annee_scolaire)


########################ELEVES INSCRITS/ACTIFS###############

#### Suppression des assignements à l'état todo eleves 
# -> supression des élements vides dans relationships/journeys/data


# Renommage de colonne pour éviter les doublons
account.rename(columns={"id": "id_account"}, inplace=True)
assignement.rename(columns={"id": "id_assignement", "attributes/createdAt": "assignementCreatedAt",
 "attributes/updatedAt": "assignementUpdatedAt"}, inplace=True)


# Join account/assignement pour les élèves
data_account_assignement_eleve= pd.merge(account, assignement, left_on=["id_account"], right_on=["attributes/accountUuid"])


# Suppression des assignements à l'état todo
todo_index=[]
todo_index=data_account_assignement_eleve[data_account_assignement_eleve["attributes/status"]=="todo"].index
#print(todo_index)

data_account_assignement_eleve = data_account_assignement_eleve.drop(todo_index)
data_account_assignement_eleve.sort_values("assignementUpdatedAt", ascending=True, inplace=True)
data_account_assignement_eleve.reset_index(drop = True, inplace = True)
#print("Nb valeurs uniques id_account : " ,len(data_account_assignement_eleve["id_account"].unique()))

###Comptage élèves

nb_eleves=0
nb_eleves_inscrits=[]

eleve_avec_assignement=0
nb_eleves_utilisateurs=[]

classes_actives=0
nb_classes_actives=[]
liste_classes_actives=[]
sauvgarde_classes_actives=[] #servira plus tard pour les etablisements actifs

# Création seuils années scolaires
seuil_account=recuperation_annee_scolaire(account, "attributes/createdAt")[1]
seuil_assignement=recuperation_annee_scolaire(data_account_assignement_eleve, "assignementCreatedAt")[1]
seuil_precedent_acc=0
seuil_precedent_ass=0
#print(seuil_assignement, seuil_account)


for seuil_acc, seuil_ass in zip(seuil_account, seuil_assignement) :

    for role,student,classe in zip(account.loc[seuil_precedent_acc:seuil_acc, "relationships/roles/data"], 
                            account.loc[seuil_precedent_acc:seuil_acc, "id_account"],
                            account.loc[seuil_precedent_acc:seuil_acc, "relationships/organizations/data"]):
        
        if re.match(r"(\[{'type': 'role', 'id': 'user:eleve'}])(.)*", role):
            nb_eleves+=1       
            if student in (data_account_assignement_eleve.loc[seuil_precedent_ass:seuil_ass, "attributes/accountUuid"]).tolist():
                eleve_avec_assignement+=1
                if classe not in liste_classes_actives:
                    #print(ast.literal_eval(classe)[0]["id"])
                    classes_actives+=1
                    liste_classes_actives.append(classe)
                    sauvgarde_classes_actives.append(ast.literal_eval(classe)[0]["id"])
                    

    # print(nb_profs, nb_eleves, eleve_avec_assignement)
    
    nb_eleves_inscrits.append(nb_eleves)
    nb_eleves_utilisateurs.append(eleve_avec_assignement)
    nb_classes_actives.append(classes_actives)
    
    nb_eleves=0
    eleve_avec_assignement=0
    classes_actives=0
    liste_classes_actives=[]


    
    seuil_precedent_acc=seuil_acc
    seuil_precedent_ass=seuil_ass
#print(nb_eleves_inscrits)
#print(nb_eleves_utilisateurs)


########################PROFESSEURS INSCRITS/ACTIFS###############

#### Join account/assignement pour les profs

data_account_assignement_profs= pd.merge(account, assignement, left_on=["id_account"], right_on=["attributes/ownerUuid"])
index_eleve_to_drop=[]

for k in range (data_account_assignement_profs["relationships/roles/data"].shape[0]):
    #print(data_account_assignement_profs["relationships/roles/data"][k])
    if data_account_assignement_profs["relationships/roles/data"][k] == "[{'type': 'role', 'id': 'user:eleve'}]":
        index_eleve_to_drop.append(k)

#print(index_eleve_to_drop)
data_account_assignement_profs.drop(index_eleve_to_drop, inplace=True)
data_account_assignement_profs.reset_index(drop = True, inplace = True)
todo_index=[]

todo_index=data_account_assignement_profs[data_account_assignement_profs["attributes/status"]!="todo"].index
data_account_assignement_profs.drop(todo_index, inplace=True)
data_account_assignement_profs.reset_index(drop = True, inplace = True)

# print(data_account_assignement_profs['relationships/roles/data'].value_counts())
# print(data_account_assignement_profs["attributes/status"].value_counts())

#print("Nb valeurs uniques id_account : " ,len(data_account_assignement_profs["id_account"].unique()))

###Comptage professeurs

nb_profs=0
nb_prof_inscrits=[]

profs_avec_assignement=0
nb_profs_utilisateurs=[]

# Création seuils années scolaires
seuil_account=recuperation_annee_scolaire(account, "attributes/createdAt")[1]
seuil_assignement=recuperation_annee_scolaire(data_account_assignement_profs, "assignementCreatedAt")[1]
seuil_precedent_acc=0
seuil_precedent_ass=0
#print(seuil_assignement, seuil_account)


for seuil_acc, seuil_ass in zip(seuil_account, seuil_assignement) :

    for role,prof in zip(account.loc[seuil_precedent_acc:seuil_acc, "relationships/roles/data"], 
                            account.loc[seuil_precedent_acc:seuil_acc, "id_account"]):
        
        if re.match(r"(\[{'type': 'role', 'id': 'user:enseignant'})(.)*", role):
            nb_profs+=1     
            if prof in (data_account_assignement_profs.loc[seuil_precedent_ass:seuil_ass, "attributes/ownerUuid"]).tolist():
                profs_avec_assignement+=1
    # print(nb_profs, nb_profs, profs_avec_assignement)
    
    nb_prof_inscrits.append(nb_profs)
    nb_profs_utilisateurs.append(profs_avec_assignement)
    
    nb_profs=0
    profs_avec_assignement=0
    
    seuil_precedent_acc=seuil_acc
    seuil_precedent_ass=seuil_ass

#print(nb_prof_inscrits)
#print(nb_profs_utilisateurs)


########################ETABLISEMENTS/CLASSE INSCRITS/ACTIFS###############

nb_etablisement=0
nb_etablisement_inscrits=[]

list_etat_actifs=[]
etat_actifs=0
nb_etablisement_utilisateurs=[]
sauvgarde_classes_actives=list(set(sauvgarde_classes_actives))


nb_classe=0
nb_classe_inscrits=[]


seuil_account=recuperation_annee_scolaire(account, "attributes/createdAt")[1]
seuil_etablisement=recuperation_annee_scolaire(organisazation, "attributes/createdAt")[1]
seuil_precedent_acc=0
seuil_precedent_eta=0
#print(seuil_etablisement)


for seuil_eta in seuil_etablisement :

    for lieu, test, id in zip(organisazation.loc[seuil_precedent_eta:seuil_eta, "attributes/kind"],
                         organisazation.loc[seuil_precedent_eta:seuil_eta, "relationships/parent/data/id"],
                         organisazation.loc[seuil_precedent_eta:seuil_eta, "id"]):
        
        #print(lieu)
        if lieu=="classe":
            nb_classe+=1
            if id in sauvgarde_classes_actives and test not in list_etat_actifs:
                list_etat_actifs.append(test)
                etat_actifs+=1

        if lieu=="etablissement":
            nb_etablisement+=1  

    # print(nb_etablisement, nb_classe)
    
    nb_classe_inscrits.append(nb_classe)
    nb_etablisement_inscrits.append(nb_etablisement)
    nb_etablisement_utilisateurs.append(etat_actifs)
    
    nb_classe=0
    nb_etablisement=0
    etablisement_avec_uai=0
    etat_actifs=0
    list_etat_actifs=[]

    

    seuil_precedent_acc=seuil_acc
    seuil_precedent_eta=seuil_eta


######NOM DE L'OUTIL

nom_outil =["smart_enseigno"]*len(annee_scolaire)
#print(nom_outil)

############CREATION DU CSV#################

smart_enseigno ={"nom_outil":nom_outil, "annee":annee_scolaire,
                 "nb_profs_inscrits":nb_prof_inscrits,"nb_profs_util":nb_profs_utilisateurs,
                "nb_eleves_util":nb_eleves_utilisateurs, "nb_eleves_inscrits":nb_eleves_inscrits,
                "nb_classe_inscrits":nb_classe_inscrits, "nb_classe_util": nb_classes_actives,
                "nb_établissement_inscrits":nb_etablisement_inscrits, "nb_établissement_util":nb_etablisement_utilisateurs}

#print(smart_enseigno)

smart_enseigno_ig = pd.DataFrame(smart_enseigno)
print(smart_enseigno_ig)

"""Insertion cvs indic_generaux"""
indic_generaux = pd.read_csv('bdd_commune/tables/indic_generaux.csv', sep=',')

index_to_del=[]
for k in range (indic_generaux.shape[0]):
    if indic_generaux["nom_outil"][k]=="smart_enseigno":
        index_to_del.append(k)
indic_generaux.drop(index_to_del, inplace = True)
indic_generaux.reset_index(drop = True, inplace = True)


indic_generaux = pd.concat([indic_generaux, smart_enseigno_ig])

indic_generaux.to_csv('bdd_commune/tables/indic_generaux.csv', sep=',', index=False)
#print(make_insert())

print(
    time.strftime("%H:%M:%S", time.gmtime(time.time() - start))
)  # Temps d'exexcution du script