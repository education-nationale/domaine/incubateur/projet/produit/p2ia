import pandas as pd
import numpy as np
import datetime

df = pd.read_csv('/home/jframery/data/evidenceb/csvExport/donnees_admath.csv', sep=',', encoding='utf-8')
df["Dates"] = pd.to_datetime(df["timestamp"], errors="coerce")

#Création de l'année scolaire
dt2223 = datetime.date(2023, 8, 1)
# utilisation des conditions si plusieurs années scolaires
conditions = {
    '2022-23': df['Dates'].dt.date < dt2223,
}
df['annee'] = np.select(conditions.values(), conditions.keys(), default='2023-24')

# Calcul du nombre d'élèves ayant utilisé l'outil par année scolaire
indic_evb = df[['student_id','annee']].groupby('annee').nunique().reset_index()
indic_evb.rename(columns={'student_id':'nb_eleves_util'}, inplace=True)
indic_evb['nom_outil']='evidenceb'

# Calcul du nombre de classes ayant utilisé l'outil par année scolaire
dfca = df[['class_id','annee']].groupby('annee').nunique().reset_index()
dfca.rename(columns={'class_id':'nb_classe_util'}, inplace=True)

# Calcul du nombre d'établissements ayant utilisé l'outil par année scolaire
dfea = df[['school_code','annee']].groupby('annee').nunique().reset_index()
dfea.rename(columns={'school_code':'nb_établissement_util'}, inplace=True)

# Fusion des données
indic_evb = indic_evb.merge(dfca, on='annee')
indic_evb = indic_evb.merge(dfea, on='annee')
print(indic_evb)

#indic_generaux = pd.concat([indic_generaux,indic_evb])

#indic_generaux.to_csv('/home/jframery/p2ia/bdd_commune/tables/indic_generaux.csv', sep=',', index=False)
