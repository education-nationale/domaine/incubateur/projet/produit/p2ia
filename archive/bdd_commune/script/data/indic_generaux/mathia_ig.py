import pandas as pd
import re
import time
import ast # Abstract Syntax Trees ->  représentation intermédiaire du code source
import math


start = time.time()  # Heure de début de script


mathia_2022_2023=pd.read_csv("matia/data/interim/csv_with_sub_dict/analytics_eventNav2022-2023_simple_sub_dict.csv")
mathia_2023_2024=pd.read_csv("matia/data/interim/csv_with_sub_dict/analytics_eventNav2023-2024_simple_sub_dict.csv")
traces_apprentisages_2023_2024=pd.read_csv("matia/data/interim/csv_with_sub_dict/treated_lrs_2023-2024.csv")
annee_scolaires=["2022-23","2023-24"]


#print(mathia_2022_2023.columns)
#print(mathia_2023_2024.columns)
#print(traces_apprentisages_2023_2024.columns)


###########Récupération du nombres d'élèves inscrits##############################


nb_eleves_inscrits_2022_2023=mathia_2022_2023["user_id"].nunique()
nb_eleves_inscrits_2023_2024 = mathia_2023_2024["user_id"].nunique() 

nb_eleves_inscrits=[nb_eleves_inscrits_2022_2023, nb_eleves_inscrits_2023_2024]
#print(nb_eleves_inscrits) 

# A faire avec les traces d'apprentisages pour une précision plus fine :

# nb_eleves_inscrits_2023 = traces_apprentisages_2023_2024["personaIdentifier/$oid"].nunique()


###########Récupération du nombres d'élèves actifs ##############################

nb_actifs=[] #Liste du nb d'élèves actifs par années scolaires

#### ANNEE SCOLAIRE 2022_2023 #####
actifs=[] #liste id des actifs

for k in range(mathia_2022_2023.shape[0])  :
    if not pd.isna(mathia_2022_2023["params/exerciceName"][k]) and  mathia_2022_2023["user_id"][k] not in actifs  :
        actifs.append(mathia_2022_2023["user_id"][k])

#print(len(actifs))
nb_actifs.append(len(actifs))



#### ANNEE SCOLAIRE 2023_2024 #####
actifs =[]

for k in range(mathia_2023_2024.shape[0])  :
    if not pd.isna(mathia_2023_2024["params/exerciceName"][k]) and  mathia_2023_2024["user_id"][k] not in actifs  :
        actifs.append(mathia_2023_2024["user_id"][k])

#print(len(actifs))
nb_actifs.append(len(actifs))


#print(nb_actifs)


#####RECUPERATION DES CLASSES ACTIFS##############

#Manque 2022-2023 -> trace d'apprentisage

nb_classes= [math.nan] #Liste du nb d'élèves actifs par années scolaires

classes=[]#liste id des actifs
for k in range(traces_apprentisages_2023_2024.shape[0]) :
    try:
        classe = ast.literal_eval(traces_apprentisages_2023_2024["statement/context"][k])["extensions"]["https://xapi&46;mathia&46;education/extensions/codeclasse"]
        # if len(str(classe))!=6:
        #     print(k)
        if classe not in classes:
            #print(classe)
            classes.append(classe)
    except KeyError:#esseyer l'autre clé codeclasse
        try:
            classe = ast.literal_eval(traces_apprentisages_2023_2024["statement/context"][k])["extensions"]["https://xapi&46;ose/extensions/codeclasse"]
            if classe not in classes:
                #print(classe)
                classes.append(classe)
        except KeyError: #Pas de clé codeclasse
            # print(ast.literal_eval(traces_apprentisages_2023_2024["statement/context"][k])["extensions"])
            continue
        continue
#print(len(classes))
nb_classes.append(len(classes))

#print(nb_classes)

###################NOM DE L'OUTIL########################

nom_outil =["mathia"]*len(annee_scolaires)
#print(nom_outil)

############CREATION DU CSV#################

mathia ={"nom_outil":nom_outil, "annee":annee_scolaires, "nb_eleves_util":nb_actifs,
                    "nb_eleves_inscrits":nb_eleves_inscrits, "nb_classe_util": nb_classes}

#print(smart_enseigno)

mathia_ig = pd.DataFrame(mathia)
print(mathia_ig)

"""Création csv"""
"""Insertion cvs indic_generaux"""

indic_generaux = pd.read_csv('bdd_commune/tables/indic_generaux.csv', sep=',')

index_to_del=[]
for k in range (indic_generaux.shape[0]):
    if indic_generaux["nom_outil"][k]=="mathia":
        index_to_del.append(k)
indic_generaux.drop(index_to_del, inplace = True)
indic_generaux.reset_index(drop = True, inplace = True)



indic_generaux = pd.concat([indic_generaux, mathia_ig])
indic_generaux.to_csv('bdd_commune/tables/indic_generaux.csv', sep=',', index=False)
#print(make_insert())


print(
    time.strftime("%H:%M:%S", time.gmtime(time.time() - start))
)  # Temps d'exexcution du script



