import pandas as pd
import numpy as np
import datetime

# Récupération de l'historique des données
indic_generaux = pd.read_csv('../tables/indic_generaux.csv', sep=',')

#Récupération / transformation des données Lalilo
df = pd.read_csv('/home/jframery/data/lalilo/data/usage/student_usage_daily_no_demo.csv', sep=',', encoding='utf-8')
df["Dates"] = pd.to_datetime(df["DAY_DATE"], errors="coerce")

#Calcul du nombre d'élèves utilisateurs

#Dates fin d'années scolaires
dt2223 = datetime.date(2023, 8, 1)
# utilisation des conditions si plusieurs années scolaires
conditions = {
    '2022-23': df['Dates'].dt.date < dt2223,
}
df['annee'] = np.select(conditions.values(), conditions.keys(), default='2023-24')
# Calcul du nombre d'élèves ayant utilisé l'outil par année scolaire
elev_util = df[['STUDENT_ID','annee']].groupby('annee').nunique().reset_index()
elev_util.rename(columns={'STUDENT_ID':'nb_eleves_util'},inplace=True)

#Calcul du nombre de profs actifs par année scolaire
prof_util = df[['PRIMARY_TEACHER_ID','annee']].groupby('annee').nunique().reset_index()
prof_util.rename(columns={'PRIMARY_TEACHER_ID':'nb_profs_util'}, inplace=True)

# Calcul du nombre de profs inscrits
dfpi = pd.read_csv('/home/jframery/data/lalilo/data/usage/usage_for_french_academias.csv', sep=',', encoding='utf-8')
dfpi = dfpi[['PRIMARY_TEACHER_ID','TEACHER_SIGNIN_DATE']].drop_duplicates()
dfpi["Dates_insc"] = pd.to_datetime(dfpi["TEACHER_SIGNIN_DATE"], errors="coerce")
df22 = dfpi[dfpi['Dates_insc'].dt.date < dt2223]
df22['annee'] = '2022-23'
dfpi['annee'] = '2023-24'
dfpi2 = pd.concat([dfpi,df22])
prof_insc = dfpi2['annee'].value_counts().reset_index()
prof_insc.rename(columns={'count':'nb_profs_inscrits'},inplace=True)

# Calcul du nombre d'écoles et classes actives
dfea = df[['SCHOOL_ID','annee']].drop_duplicates()
etab_util = dfea['annee'].value_counts().reset_index()
etab_util.rename(columns={'count':'nb_établissement_util'}, inplace=True)

dfca = df[['CLASS_ID','annee']].drop_duplicates()
ecol_util = dfca['annee'].value_counts().reset_index()
ecol_util.rename(columns={'count':'nb_classe_util'}, inplace=True)

#Intégration des données de Lalilo dans indic_genraux
dfig = prof_util.merge(prof_insc, on='annee')
dfig = dfig.merge(elev_util, on='annee')
dfig = dfig.merge(etab_util, on='annee')
dfig = dfig.merge(ecol_util, on='annee')
dfig['nom_outil']='lalilo'

indic_generaux = pd.concat([indic_generaux,dfig])
indic_generaux.to_csv('/home/jframery/p2ia/bdd_commune/tables/indic_generaux.csv',sep=',', index=False, encoding='utf-8')