from engine import DatabaseEngine


def exe_sql_postgres(path: str, **kwargs):
    """
    Function to read a SQL script, format it with the given parameters and
    execute it.
    """
    db = DatabaseEngine()
    try:
        with open(path) as file:
            query = file.read().format(**kwargs)
            db.run_query(query)
    except Exception as e:
        print(e)
        raise e
