CREATE TABLE IF NOT EXISTS staging.parcours
(
    id_parcours SERIAL PRIMARY KEY,
    nom_outil VARCHAR(50) NOT NULL,
    id_elev VARCHAR(50) NOT NULL,
    parcours TEXT,
    nb_activites INTEGER,
    nb_activites_dup INTEGER,
    nb_competences INTEGER,
    nb_comp_acquises INTEGER,
    lst_competences TEXT,
    annee VARCHAR(9),
    dt_debut TIMESTAMP,
    dt_fin TIMESTAMP,
    nb_verbes INTEGER
);
