CREATE TABLE IF NOT EXISTS staging.indic_generaux
(
    annee VARCHAR(8),
    nb_eleves_util INTEGER,
    nom_outil VARCHAR(50),
    nb_classe_util INTEGER,
    nb_établissement_util INTEGER,
    nb_établissement_inscrits INTEGER,
    nb_classe_inscrits INTEGER,
    nb_eleves_inscrits INTEGER,
    nb_profs_util INTEGER,
    nb_profs_inscrits INTEGER,
    CONSTRAINT indic_generaux_annee_nom_outil_unique UNIQUE (annee, nom_outil)
);