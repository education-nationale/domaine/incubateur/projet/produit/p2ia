TRUNCATE TABLE staging.parcours;

INSERT INTO staging.parcours (nom_outil, id_elev, parcours, nb_activites, nb_activites_dup, nb_competences, nb_comp_acquises, lst_competences, annee, dt_debut, dt_fin, nb_verbes)
SELECT
    'navi' as nom_outil,
    student_id as id_elev,
    STRING_AGG(DISTINCT content_id::text, ',') AS parcours,
    COUNT(DISTINCT content_id) AS nb_activites,
    COUNT(content_id) AS nb_activites_dup,
    COUNT(DISTINCT knowledge_node_id) AS nb_competences,
    COUNT(
        DISTINCT
        CASE
            WHEN success = TRUE THEN knowledge_node_id
            ELSE NULL
        END
    ) AS nb_comp_acquises,
    STRING_AGG(DISTINCT knowledge_node_id::text, ',') AS lst_competences,
    CASE
        WHEN EXTRACT(MONTH FROM created_at::timestamp) > 7 OR (EXTRACT(MONTH FROM created_at::timestamp) = 7 AND EXTRACT(DAY FROM created_at::timestamp) >= 1)
            THEN TO_CHAR(created_at::timestamp, 'YYYY') || '-' || TO_CHAR(created_at::timestamp + INTERVAL '1 year', 'YYYY')
        ELSE TO_CHAR(created_at::timestamp - INTERVAL '1 year', 'YYYY') || '-' || TO_CHAR(created_at::timestamp, 'YYYY')
    END AS annee,
    MIN(created_at::timestamp) AS dt_debut,
    MAX(updated_at::timestamp) AS dt_fin,
    NULL AS nb_verbes
FROM
    raw.events
GROUP BY
    id_elev,
    annee;
