INSERT INTO {env_cible}.parcours (nom_outil, id_elev, parcours, nb_activites,
    nb_activites_dup, nb_competences, nb_comp_acquises, lst_competences, annee,
    dt_debut, dt_fin, nb_verbes)
SELECT nom_outil, id_elev, parcours, nb_activites, nb_activites_dup, nb_competences,
    nb_comp_acquises, lst_competences, annee,
    to_timestamp(dt_debut, 'DD-MM-YYYY HH24:MI:SS'),
    to_timestamp(dt_fin, 'DD-MM-YYYY HH24:MI:SS'),
    nb_verbes
FROM {env_source}.{table_name}
ON CONFLICT (id_elev, annee) DO UPDATE SET
    parcours = EXCLUDED.parcours,
    nb_activites = EXCLUDED.nb_activites,
    nb_activites_dup = EXCLUDED.nb_activites_dup,
    nb_competences = EXCLUDED.nb_competences,
    nb_comp_acquises = EXCLUDED.nb_comp_acquises,
    lst_competences = EXCLUDED.lst_competences,
    dt_debut = EXCLUDED.dt_debut,
    dt_fin = EXCLUDED.dt_fin,
    nb_verbes = EXCLUDED.nb_verbes;
