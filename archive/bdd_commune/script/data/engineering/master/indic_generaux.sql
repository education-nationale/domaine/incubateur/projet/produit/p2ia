INSERT INTO {env_cible}.indic_generaux (
    annee, nb_eleves_util, nom_outil, nb_classe_util, nb_établissement_util,
    nb_établissement_inscrits, nb_classe_inscrits, nb_eleves_inscrits,
    nb_profs_util, nb_profs_inscrits
)
SELECT
    annee, nb_eleves_util, nom_outil, nb_classe_util, nb_établissement_util,
    nb_établissement_inscrits, nb_classe_inscrits, nb_eleves_inscrits,
    nb_profs_util, nb_profs_inscrits
FROM {env_source}.{table_name}
ON CONFLICT (annee, nom_outil) DO UPDATE SET
    nb_eleves_util = EXCLUDED.nb_eleves_util,
    nb_classe_util = EXCLUDED.nb_classe_util,
    nb_établissement_util = EXCLUDED.nb_établissement_util,
    nb_établissement_inscrits = EXCLUDED.nb_établissement_inscrits,
    nb_classe_inscrits = EXCLUDED.nb_classe_inscrits,
    nb_eleves_inscrits = EXCLUDED.nb_eleves_inscrits,
    nb_profs_util = EXCLUDED.nb_profs_util,
    nb_profs_inscrits = EXCLUDED.nb_profs_inscrits;
