CREATE TABLE master.lalilo AS
SELECT
    CAST("STUDENT_ID" AS VARCHAR(100)) AS student_id,
    CAST("CREATED_AT" AS TIMESTAMP) AS created_at,
    CAST("TRACE_ID" AS INTEGER) AS trace_id,
    CAST("MODE" AS VARCHAR(50)) AS mode,
    CAST("TRACE_LESSON_ID" AS INTEGER) AS trace_lesson_id,
    CAST("TEMPLATE_NAME" AS VARCHAR(50)) AS template_name,
    CAST("LEVEL" AS INTEGER) AS level,
    CAST("LEARNING_OBJECTIVE_ID" AS INTEGER) AS learning_objective_id,
    CAST("TAB_NAME" AS VARCHAR(50)) AS tab_name,
    CAST("IS_VALIDATING_TRACE" AS BOOLEAN) AS is_validating_trace,
    CAST("IS_REMEDIATION" AS BOOLEAN) AS is_remediation,
    CAST("NB_GOOD_ANSWERS" AS INTEGER) AS nb_good_answers,
    CAST("NB_WRONG_ANSWERS" AS INTEGER) AS nb_wrong_answers,
    CAST("NB_ANSWERS" AS INTEGER) AS nb_answers,
    CAST("SUCCESS_RATE" AS NUMERIC(6, 1)) AS success_rate,
    CAST("REPORTED_TIME_SPENT_IN_SECONDS_IN_TRACE" AS INTEGER) AS time_taken_in_seconds
FROM raw.lalilo;
