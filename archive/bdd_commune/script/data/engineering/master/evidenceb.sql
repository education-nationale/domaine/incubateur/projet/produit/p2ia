CREATE TABLE master.evidenceb AS
SELECT
    CAST("product_family" AS VARCHAR(50)) AS product_family,
    CAST("product" AS VARCHAR(50)) AS product,
    CAST("is_success" AS BOOLEAN) AS is_success,
    CAST("school_code" AS VARCHAR(50)) AS school_code,
    CAST("student_id" AS UUID) AS student_id,
    CAST("class_id" AS UUID) AS class_id,
    CAST("module_id" AS UUID) AS module_id,
    CAST("module_title" AS VARCHAR(255)) AS module_title,
    CAST("objective_id" AS UUID) AS objective_id,
    CAST("objective_title" AS VARCHAR(255)) AS objective_title,
    CAST("activity_id" AS UUID) AS activity_id,
    CAST("activity_title" AS VARCHAR(255)) AS activity_title,
    CAST("exercise_id" AS UUID) AS exercise_id,
    CAST("timestamp" AS TIMESTAMP) AS timestamp,
    CAST("score" AS NUMERIC(3, 2)) AS score,
    EXTRACT(EPOCH FROM "time_taken"::INTERVAL) AS time_taken_in_seconds
FROM raw.evidenceb;
