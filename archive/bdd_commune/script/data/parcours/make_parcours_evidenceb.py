import pandas as pd
import numpy as np
import datetime
import psycopg2

# Extraction des données
df = pd.read_csv('/home/jframery/data/evidenceb/csvExport/donnees_admath.csv', sep=',', encoding='utf-8')

# Modification des types de données des dates
df = df[df['timestamp'].isna() == False]
df['timestamp'] = df['timestamp'].str[:19]
df["timestamp"] = pd.to_datetime(df["timestamp"], format='%Y-%m-%d %H:%M:%S')
df['time_taken'] = pd.to_timedelta(df['time_taken'])

# Création de l'année scolaire

#Dates fin d'années scolaires
dt2223 = datetime.date(2023, 8, 1)

# utilisation des conditions si plusieurs années scolaires
conditions = {
    '2022-23': df['timestamp'].dt.date < dt2223,
}
df['annee'] = np.select(conditions.values(), conditions.keys(), default='2023-24')

# Tri et déduplication des activités
df.sort_values(['student_id','timestamp','exercise_id'], inplace=True)
df2 = df.drop_duplicates(['student_id','exercise_id'])

# Création de la liste d'activité de l'élève
df_aggr = df2.groupby(['student_id','annee'])['exercise_id'].apply(list).reset_index()

# Création nom_outil
df_aggr['nom_outil'] = 'evidenceb'

# Date de début de parcours
df_dtdebparc = df[['student_id','annee','timestamp']].groupby(['student_id','annee']).min().reset_index()
df_dtdebparc['timestamp'] = df_dtdebparc['timestamp'].dt.strftime('%d-%m-%Y:%H:%M:%S')
df_dtdebparc = df_dtdebparc[df_dtdebparc['timestamp'].isna() == False]

# Date de fin de parcours
df_dtfinparc = df[['student_id','annee','timestamp']].groupby(['student_id','annee']).max().reset_index()
df_dtfinparc['timestamp'] = df_dtfinparc['timestamp'].dt.strftime('%d-%m-%Y:%H:%M:%S')
df_dtfinparc = df_dtfinparc[df_dtfinparc['timestamp'].isna() == False]

# Ajout des dates dans df_aggr
df_aggr = df_aggr.merge(df_dtdebparc, on=['student_id','annee'], how='left')
df_aggr.rename(columns={'timestamp':'dt_debut'}, inplace=True)
df_aggr = df_aggr.merge(df_dtfinparc, on=['student_id','annee'], how='left')
df_aggr.rename(columns={'timestamp':'dt_fin'}, inplace=True)

# Nb activités par élève et année
df_nba = df[['student_id','annee','exercise_id']].groupby(['student_id','annee']).count().reset_index()

# Nb activités dédupliquées par élève et année
df_nbad = df[['student_id','annee','exercise_id']].drop_duplicates().groupby(['student_id','annee']).count().reset_index()

# Ajout des décomptes dans df_aggr
df_aggr = df_aggr.merge(df_nba, on=['student_id','annee'], how='left')
df_aggr.rename(columns={'exercise_id':'nb_activites'}, inplace=True)
df_aggr = df_aggr.merge(df_nbad, on=['student_id','annee'], how='left')
df_aggr.rename(columns={'exercise_id':'nb_activites_dup'}, inplace=True)

#Connection à Postgre
try:
    conn = psycopg2.connect(
        user=USER_DB,
        password=PASSWORD,
        host=HOST,
        port=PORT,
        database=DATABASE
    )
    cur = conn.cursor()