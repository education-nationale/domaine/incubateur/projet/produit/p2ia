import pandas as pd
import re
import ast # Abstract Syntax Trees -> permet de travailler avec les arbres de syntaxe abstraite, qui sont une représentation intermédiaire du code source
import time

start = time.time()  # Heure de début de script

################################################################################################
"""Création de la table real_journey ne possédant que les activitées réellement commencées (pas à l'état todo)"""


journey = pd.read_csv("smart-enseigno/data/interim/reduce_csv/journey_deep_sub_dict.csv")

todo_index=[]


todo_index=journey[journey["attributes/status"]=="todo"].index
#print(todo_index)


real_journey = journey.drop(todo_index)
real_journey.sort_values("attributes/updatedAt", ascending=True, inplace=True)
real_journey.reset_index(drop = True, inplace = True)
# print(real_journey)
# print(real_journey["attributes/status"].value_counts())

################################################################################################
"""Renommage de colonne pour éviter les doublons"""

assignement= pd.read_csv("smart-enseigno/data/interim/reduce_csv/assignment_complet_deep_sub_dict.csv")
account= pd.read_csv("smart-enseigno/data/interim/reduce_csv/account_deep_sub_dict.csv")

account.rename(columns={"id": "id_account"}, inplace=True)
assignement.rename(columns={"id": "id_assignement", "attributes/createdAt": "assignementCreatedAt",
"attributes/updatedAt": "assignementUpdatedAt"}, inplace=True)
################################################################################################
"""Join account/assignement pour les élèves"""
rel_ass_acc= pd.merge(account, assignement, left_on=["id_account"], right_on=["attributes/accountUuid"])
rel_ass_acc
################################################################################################
"""Récupération des colonnes utiles"""
data_account_assignement=rel_ass_acc[["id_account", "relationships/roles/data", "relationships/organizations/data","attributes/uai", 
            "id_assignement", "assignementCreatedAt", "assignementUpdatedAt", 
            "attributes/status", "attributes/reason", "attributes/rank", "relationships/assignmentChain/data/id",
            "attributes/softDelete", "relationships/journeys/data", ]]
################################################################################################
"""Suppression des assignements à l'état todo -> supression des élements vides dans relationships/journeys/data"""
todo_index=[]
todo_index=data_account_assignement[data_account_assignement["attributes/status"]=="todo"].index
#print(todo_index)

data_account_assignement = data_account_assignement.drop(todo_index)
data_account_assignement.sort_values("assignementUpdatedAt", ascending=True, inplace=True)
data_account_assignement.reset_index(drop = True, inplace = True)
################################################################################################
"""Récupération de l'id journey associé"""
for k in range (data_account_assignement.shape[0]):
    size=len(data_account_assignement.loc[data_account_assignement.index[k], "relationships/journeys/data"])
    data_account_assignement.loc[data_account_assignement.index[k], "relationships/journeys/data"] = data_account_assignement.loc[data_account_assignement.index[k], "relationships/journeys/data"][28:size-3]
data_account_assignement["relationships/journeys/data"]
################################################################################################
"""Création du dataframe récupération des parcours en fonction des id élèves"""
parcours= pd.merge(real_journey, data_account_assignement, left_on=["id"], right_on=["relationships/journeys/data"])

parcours.rename(columns={"id": "id_journey", "attributes/status_x": "journey_status", "attributes/status_y": "assignment_status",
                        "attributes/createdAt":"journeyCreatedAt", 
                        "attributes/updatedAt":"journeyUpdatedAt" }, inplace=True)
################################################################################################
"""Récupération des id journeyItem des exercices constitutifs du parcours """
path=[]
all_path=[]
for i in range (parcours.shape[0]):
    #print(parcours["relationships/journeyItems/data"][i])
    dico_exercices=ast.literal_eval(parcours["relationships/journeyItems/data"][i]) # str -> list
    #print(dico_exercices)
    for id_exercice in dico_exercices:
        # print(id_exercice["id"])
        path.append(id_exercice["id"])
    # print(path)
    all_path.append(path)
    path=[]

parcours["relationships/journeyItems/data"]=all_path

##############################################################################################
"""Création du dataframe des parcours élèves"""

eleve=[]
nb_verbs=[]
all_parcours=[]

for i in range (parcours["id_account"].shape[0]):
    
    if parcours["id_account"][i] not in eleve:
        current_eleve=parcours["id_account"][i]
        complete_parcours=parcours["relationships/journeyItems/data"][i]
        eleve.append(current_eleve)
        #print(complete_parcours)
        if parcours["journey_status"][i] == "completed":
        
            nb_actual_verbs = 2
        else:
            nb_actual_verbs = 1
        
        for j in range(i+1, parcours["id_account"].shape[0]):
            if current_eleve == parcours["id_account"][j]:
                complete_parcours=complete_parcours + parcours["relationships/journeyItems/data"][j]
            
            if nb_actual_verbs!=2 and parcours["journey_status"][j] == "completed":
                nb_actual_verbs = 2
        #print(complete_parcours)
        nb_verbs.append(nb_actual_verbs)
        all_parcours.append(complete_parcours)

# print(len(eleve))
# print(len(all_parcours))
# print(type(all_parcours[0]))


parcours_par_eleve=pd.DataFrame({"id_elev" : eleve, "parcours" : all_parcours,"nb_verbs" : nb_verbs})
#print(parcours_par_eleve["parcours"][0])

############################################################################################################
"""Travail sur journeyItem, tableau des activités"""
journeyItem=pd.read_csv("C:/Users/atellech/Documents/p2ia/smart-enseigno/data/interim/reduce_csv/journeyItem_deep_sub_dict.csv")

journeyItem.rename(columns={"id": "id_activite" }, inplace=True)

#print(journeyItem.columns)
journeyItem=journeyItem[["id_activite", "attributes/updatedAt", "relationships/journey/data/id", "attributes/title"]]

############################################################################################################
"""Récupération des parcours par années scolaires via date de début et fin de parcours"""
dt_debut=[]
dt_fin=[]
annee_scolaire_min=[]
annee_scolaire_max=[]
new_lines=[] #Nouvelles lignes parcours sous forme de dataframes
lignes_to_drop=[] #lignes contenant plusieurs parcours par années scolaires dans parcours_par_eleve

for k in range(parcours_par_eleve.shape[0]):
    date_activite=journeyItem[journeyItem["id_activite"].isin(parcours_par_eleve["parcours"][k])]
    # print(date_activite[["id_activite","attributes/updatedAt"]])
    date_max=date_activite["attributes/updatedAt"].max()
    date_min=date_activite["attributes/updatedAt"].min()
    # print(date_max, date_min)


    ########Séparation des parcours élèves selon l'année scolaire

    #Cas sur une année civile
    
    if int(date_max[0:4])-int(date_min[0:4])==0:
        if int(date_max[5:7])<=9 :
            pass
        elif int(date_min[5:7])>=9 and int(date_max[5:7])>=9:
            pass
        else: #Cas deux années scolaires différentes, parcours sur une seule année civile
            # print(date_activite[["id_activite","attributes/updatedAt"]])  
            # print(date_min, date_max)
            lignes_to_drop.append(k)
            annee_scolaire_1=[]
            dates_1=[]
            annee_scolaire_2=[]
            dates_2=[]
            deux_annees_scolaires=[]
            for id, date in zip(date_activite["id_activite"],date_activite["attributes/updatedAt"]):
                # print(id)
                if int(date[5:7])<9:
                    annee_scolaire_1.append(id)
                    dates_1.append(date)
                else:    
                    annee_scolaire_2.append(id)
                    dates_2.append(date)
            
            #Nouvelles dates min et max parcours
            date_min_1=min(dates_1)
            date_min_2=min(dates_2)
            date_max_1=max(dates_1)
            date_max_2=max(dates_2)
            # print(annee_scolaire_1)
            # print(annee_scolaire_2)
            #Nouvelles lignes parcours par élèves
            deux_annees_scolaires.append(annee_scolaire_1)
            df1=pd.DataFrame({"id_elev":parcours_par_eleve["id_elev"][k], "parcours":deux_annees_scolaires, 
                              "dt_debut":date_min_1,"dt_fin":date_max_1,
                              "nb_verbs":parcours_par_eleve["nb_verbs"][k]})
            # print(df1)
            new_lines.append(df1)
            
            deux_annees_scolaires=[]
            deux_annees_scolaires.append(annee_scolaire_2)
            df2=pd.DataFrame({"id_elev":parcours_par_eleve["id_elev"][k], "parcours":deux_annees_scolaires, 
                              "dt_debut":date_min_2,"dt_fin":date_max_2,
                              "nb_verbs":parcours_par_eleve["nb_verbs"][k]})
            # print(df2)
            new_lines.append(df2)     
                
    elif int(date_max[0:4])-int(date_min[0:4])==1:
        if int(date_min[5:7])>=9 and int(date_max[5:7])<9:
            pass
        else:
        
            lignes_to_drop.append(k)
            def get_school_year(date): #Permet de créer une colonne année scolaire
                if int(date[5:7]) >= 9:
                    return f"{int(date[0:4])}-{int(date[2:4]) + 1}"
                else:
                    return f"{int(date[0:4]) - 1}-{int(date[2:4])}"        
            
            date_activite["année_scolaire"] = date_activite["attributes/updatedAt"].apply(get_school_year)

            #print(date_activite[["id_activite","attributes/updatedAt", "année_scolaire"]])
            grouped = date_activite.groupby("année_scolaire") #dataframes par années scolaires
            # for name, group in grouped:
            #     print(f"Année scolaire: {name}")
            #     print(group[["id_activite","attributes/updatedAt", "année_scolaire"]])
            for dataframe in grouped:
                # print(dataframe[1].loc[:,"id_activite"].tolist())
                # print(dataframe[1].loc[:,"attributes/updatedAt"].tolist())
                parcours_annee=[dataframe[1].loc[:,"id_activite"].tolist()]
                dates=dataframe[1].loc[:,"attributes/updatedAt"].tolist()
            
                min_date=min(dates)
                max_date=max(dates)
                
                df=pd.DataFrame({"id_elev":parcours_par_eleve["id_elev"][k], "parcours":parcours_annee, 
                        "dt_debut":min_date,"dt_fin":max_date,
                        "nb_verbs":parcours_par_eleve["nb_verbs"][k]})
                # print(df)
                new_lines.append(df)
    
    
    elif int(date_max[0:4])-int(date_min[0:4])>1:        
        lignes_to_drop.append(k)
        
        def get_school_year(date): #Permet de créer une colonne année scolaire
            if int(date[5:7]) >= 9:
                return f"{int(date[0:4])}-{int(date[2:4]) + 1}"
            else:
                return f"{int(date[0:4]) - 1}-{int(date[2:4])}"        
        
        date_activite["année_scolaire"] = date_activite["attributes/updatedAt"].apply(get_school_year)

        #print(date_activite[["id_activite","attributes/updatedAt", "année_scolaire"]])
        grouped = date_activite.groupby("année_scolaire") #dataframes par années scolaires
        # for name, group in grouped:
        #     print(f"Année scolaire: {name}")
        #     print(group[["id_activite","attributes/updatedAt", "année_scolaire"]])
        for dataframe in grouped:
            # print(dataframe[1].loc[:,"id_activite"].tolist())
            # print(dataframe[1].loc[:,"attributes/updatedAt"].tolist())
            parcours_annee=[dataframe[1].loc[:,"id_activite"].tolist()]
            dates=dataframe[1].loc[:,"attributes/updatedAt"].tolist()
        
            min_date=min(dates)
            max_date=max(dates)
            
            df=pd.DataFrame({"id_elev":parcours_par_eleve["id_elev"][k], "parcours":parcours_annee, 
                    "dt_debut":min_date,"dt_fin":max_date,
                    "nb_verbs":parcours_par_eleve["nb_verbs"][k]})
            # print(df)
            new_lines.append(df)
        
    dt_debut.append(date_min)
    dt_fin.append(date_max)


parcours_par_eleve.insert(loc=2, column="dt_debut", value =dt_debut)
parcours_par_eleve.insert(loc=3, column="dt_fin", value =dt_fin)
# print(len(dt_debut))
# print(len(dt_fin))

# print(len(new_lines))
for dataframe in new_lines:
    #print(dataframe)
    parcours_par_eleve=pd.concat([parcours_par_eleve, dataframe], ignore_index=True)
# parcours_par_eleve = pd.concat([parcours_par_eleve] + new_lines, ignore_index=True)

# print(parcours_par_eleve.shape)

parcours_par_eleve.drop(lignes_to_drop, inplace=True)
parcours_par_eleve.reset_index(drop = True, inplace = True)

parcours_par_eleve


###############################################################################################################
"""Récupération des nom des activitées dans les parcours"""
#Récupérations des nom des activitées 

name_activities=[]
all_activity=[]
dico={}
for k in range(journeyItem.shape[0]):
    if pd.isna(journeyItem["attributes/title"][k]):
        dico[journeyItem["id_activite"][k]]="Nom manquant"
    else: 
        dico[journeyItem["id_activite"][k]]=journeyItem["attributes/title"][k]

for id_activite in (parcours_par_eleve["parcours"]):#liste de liste exo
    # print(id_activite)
    for i in range(len(id_activite)):#liste d'exo
        # print(id_activite[i])
        name_activities.append(dico[id_activite[i]])
    
    all_activity.append(name_activities)
    name_activities=[]


parcours_par_eleve["parcours"]=all_activity


#Etude sur les répétitions d'exercices
nb_activities_dup=[]
nb_activities=[]
c=0
index=0
for activites in all_activity:
    nb_total=len(activites)
    nb_activities_dup.append(nb_total)
    
    if len(set(activites))!=nb_total:
        # print(index, activites)
        duplicat= nb_total -len(set(activites))
        nb_activities.append(nb_total - duplicat)
        duplicat=0
        c=c+1
        index+=1

    else:
        nb_activities.append(nb_total)
        index+=1
#print(c)


parcours_par_eleve.insert(loc=4, column="nb_activities", value =nb_activities)
parcours_par_eleve["nb_activities_dup"]= nb_activities_dup

# print(parcours_par_eleve["parcours"][0])
###############################################################################################################
"""Supression des doublons deux à deux dans la liste des activitées"""
all_non_doublons=[]
for activites in all_activity:
    non_doublons=[activites[0]]
    for k in range (1, len(activites)):
        if activites[k]!=activites[k-1]:
            non_doublons.append(activites[k])
    all_non_doublons.append(non_doublons)
    non_doublons=[]

# print(parcours_par_eleve["parcours"][11])
# print(parcours_par_eleve["parcours"][16])

parcours_par_eleve["parcours"]=all_non_doublons

# print(parcours_par_eleve["parcours"][11])
# print(parcours_par_eleve["parcours"][16])
###############################################################################################################
"""Nom outil"""
parcours_par_eleve.insert(loc=0, column="nom_outil", value =["smart_enseigno"]*parcours_par_eleve.shape[0])

#print(parcours_par_eleve.columns)
############################################################################################################
"""Années scolaires"""
annees_scolaires=[]
for annee in parcours_par_eleve["dt_debut"]:
    #print(annee[6],int(annee[0:4])-1, annee[2:4])
    if int(annee[6])<9 and int(annee[5])==0:
        annee_x=int(annee[0:4]) -1
        annee_y= annee[2:4]
        annee_scolaire=str(annee_x)+"-"+annee_y
        annees_scolaires.append(annee_scolaire)
    else:
        annee_x=annee[0:4]
        annee_y= int(annee[2:4])+1
        annee_scolaire=annee_x+"-"+str(annee_y)
        annees_scolaires.append(annee_scolaire)
#print(annees_scolaires)

parcours_par_eleve.insert(loc=1, column="annee", value =annees_scolaires)

############################################################################################################
"""Création csv"""
#CSV smart-enseino
parcours_par_eleve.to_csv('smart-enseigno/data/processed/parcours.csv', sep=',', index=False)

#CSV général
parcours_finaux = pd.read_csv('bdd_commune/tables/parcours.csv', sep=',')

index_to_del=[]
for k in range (parcours_finaux.shape[0]):
    if parcours_finaux["nom_outil"][k]=="smart_enseigno":
        index_to_del.append(k)
parcours_finaux.drop(index_to_del, inplace = True)
parcours_finaux.reset_index(drop = True, inplace = True)



parcours_finaux = pd.concat([parcours_finaux, parcours_par_eleve])

parcours_finaux.to_csv('bdd_commune/tables/parcours.csv', sep=',', index=False)
############################################################################################################
print(
time.strftime("%H:%M:%S", time.gmtime(time.time() - start))
)  # Temps d'execution du script
