import argparse
import pandas as pd
from engine import DatabaseEngine


def load_to_postgres(data: pd.DataFrame or str, db_name: str = 'temp_table',
                     env: str = 'raw', chunksize: int = 100000,
                     path: str = 'bdd_commune/script/data/engineering'
                     ) -> bool:
    """
    Load data to the database

    :param env: environment
    :param db_name: database name
    :param data: data name
    :param path: path to the data
    :param chunksize: chunksize to load the data

    :return: True if the data is loaded, False otherwise
    """
    db = DatabaseEngine()
    eng = db.get_engine()
    inspector = db.get_inspector()

    # Truncate the table before loading new data
    try:
        db.run_query(f"TRUNCATE TABLE {env}.{db_name};")
        print(f'Table {db_name} truncated')
    except Exception:
        pass

    if not isinstance(data, pd.DataFrame):
        if data.split('.')[-1] == 'csv':
            try:
                data = pd.read_csv(
                    f'{path}/{env}/{data}', chunksize=chunksize, sep=';')
            except pd.errors.ParserError:
                data = pd.read_csv(
                    f'{path}/{env}/{data}', chunksize=chunksize, sep=',')
            except FileNotFoundError:
                data = pd.read_csv(
                    f'{path}/{data}', chunksize=chunksize, sep=',')
        elif data.split('.')[-1] == 'json':
            with open(f'{path}{data}') as f:
                data = pd.read_json(f, lines=True, chunksize=chunksize)
        else:
            raise ValueError('Data must be a csv or json file')
        for i, chunk in enumerate(data):
            print(f"Writing {len(chunk)} rows")
            chunk.to_sql(
                name=db_name, con=eng, if_exists='append', index=False,
                schema=env
            )
    else:
        data.to_sql(
            name=db_name, con=eng, if_exists='append', index=False, schema=env
        )

    datas = inspector.get_table_names(schema=env)
    return db_name in datas


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Load data to the database')
    parser.add_argument('--data', type=str, required=True, help='Table name')
    parser.add_argument('--db_name', type=str, required=True, help='DB name')
    parser.add_argument('--env', type=str, default='raw', help='Environment')
    parser.add_argument(
        '--path', type=str, default='bdd_commune/script/data/engineering')

    args = parser.parse_args()
    if args.data.split('.')[-1] not in ['csv', 'json']:
        raise ValueError('Data must be a csv or json file')
    if load_to_postgres(
            data=args.data, env=args.env, db_name=args.db_name,
            path=args.path):
        print('Data loaded successfully')
