import os
from dotenv import load_dotenv
from sqlalchemy import create_engine, inspect, text

load_dotenv()

HOST = os.getenv('HOST')
USER_DB = os.getenv('USER_DB')
PASSWORD = os.getenv('PASSWORD')
PORT = os.getenv('PORT')
DB = os.getenv('DATABASE')


class DatabaseEngine:
    def __init__(self):
        self.db_url = f'postgresql://{USER_DB}:{PASSWORD}@{HOST}:{PORT}/{DB}'

    def get_engine(self):
        return create_engine(self.db_url)

    def get_inspector(self):
        engine = self.get_engine()
        return inspect(engine)

    def run_query(self, query: text):
        engine = self.get_engine()
        with engine.begin() as conn:
            result = conn.execute(text(query))
        return result
