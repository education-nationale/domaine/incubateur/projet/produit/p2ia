import os
import time
import yaml
import pandas as pd
from pathlib import Path
from src.visualization import dashboard
from src.features import convert_files, deep_sub_dict_json, action_on_folder

start = time.time()  # Heure de début de script

######Création du dashboard de parcours


dashboard.analysis(
    "smart-enseigno/data/processed/parcours.csv",
    "smart-enseigno/reports",
    "smart-enseigno/reports/modele",
    folder_name="modele",
)

print(
    time.strftime("%H:%M:%S", time.gmtime(time.time() - start))
)  # Temps d'exexcution du script