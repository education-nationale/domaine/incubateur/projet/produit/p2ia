"""
Visualisation des données bruts de smart-enseigno
"""

import os
import time
import yaml
import pandas as pd
from pathlib import Path
from src.visualization import dashboard
from src.features import convert_files, deep_sub_dict_json, action_on_folder

start = time.time()  # Heure de début de script

# with open("config\config.yaml\path.yaml") as f:
#         path = yaml.safe_load(f)


# Conversion des jsonl en json

unique_files_json=[] #liste des fichiers unique à traiter
json_folder = action_on_folder.create_folder(
    "smart-enseigno/data/interim", "jsonl_to_json_deep_sub_dict"
)
DATA = "smart-enseigno/data/raw/apart_files"

for file in os.listdir(DATA):
    # print(file)
    path_file = os.path.join(DATA, file)
    # print(path_file)
    convert_files.convert_jsonl_to_json(path_file, json_folder)
    unique_files_json.append(f"{Path(file).stem}.json")
    print(unique_files_json)

# Récupération des sous-dictionnaires

unique_files_deep_sub_dict=[] #liste des fichiers unique à traiter
sub_dict_folder = action_on_folder.create_folder(
    "smart-enseigno/data/interim", "json_with_deep_sub_dict"
)
for file in unique_files_json:
    # print(file)
    path_file = os.path.join("smart-enseigno/data/interim/jsonl_to_json_deep_sub_dict", file)
    deep_sub_dict_json.get_sub_dict_from_json(path_file, sub_dict_folder)
    unique_files_deep_sub_dict.append(f"{Path(file).stem}_deep_sub_dict.json")
    print(unique_files_deep_sub_dict)

# Conversion des nouveaux json en CSV

unique_files_deep_sub_dict_csv=[] #liste des fichiers unique à traiter
csv_folder = action_on_folder.create_folder(
    "smart-enseigno/data/interim", "csv_with_deep_sub_dict"
)
for file in (unique_files_deep_sub_dict):
    #print(file)
    path_file = os.path.join("smart-enseigno/data/interim/json_with_deep_sub_dict", file)
    convert_files.convert_json_to_csv(path_file, csv_folder)
    unique_files_deep_sub_dict_csv.append(f"{Path(file).stem}.csv")
    print(unique_files_deep_sub_dict_csv)

# Suppresions des colonnes inutiles dans les csv

final_csv = action_on_folder.create_folder(
"smart-enseigno/data/interim", "reduce_csv"
)

for file in unique_files_deep_sub_dict_csv:
    
    chunks=[] #Liste des datatframes fractionnés
    print(file)
    path_file = os.path.join("smart-enseigno/data/interim/csv_with_deep_sub_dict", file)
    if path_file.endswith(".csv"):
        data = pd.read_csv(path_file, sep=",", chunksize= 6000)

    elif path_file.endswith(".xlsx") or path_file.endswith(".xls"):
        data = pd.read_excel(path_file)
        
    for chunk in data:
        print(chunk)

        for column in chunk.columns:
            missing_values_total = chunk[column].isna().sum().sum()
            
            percentage_of_missing_values = (missing_values_total / chunk[column].shape[0] * 100)
            
            if percentage_of_missing_values>99 :
                # print(column)
                # print(percentage_of_missing_values)
                del chunk[column]
        chunks.append(chunk)
        
    data= pd.concat(chunks)
    
    with open(f"{final_csv}/{file}", "w", encoding="utf-8") as csv_file:
        data.to_csv(path_or_buf=csv_file, index=False, lineterminator="")


# Visualisation des données

for file in unique_files_deep_sub_dict_csv:
    # print(file)
    path_file = os.path.join("smart-enseigno/data/interim/reduce_csv", file)
    # print(path_file)
    dashboard.analysis(
        path_file,
        "smart-enseigno/reports",
        "smart-enseigno/reports/reduce_deep_sub_dict",
        folder_name="reduce_deep_sub_dict",
    )


# print(time.time())
# print(start)
# print(time.ctime()[11:19])
# print((time.ctime(start))[11:19])
print(
    time.strftime("%H:%M:%S", time.gmtime(time.time() - start))
)  # Temps d'exexcution du script
