"""
Visualisation des données bruts de smart-enseigno
"""

import os
import time
import yaml
from src.visualization import dashboard
from src.features import convert_files, sub_dict_json, action_on_folder

start = time.time()  # Heure de début de script

# with open("config\config.yaml\path.yaml") as f:
#         path = yaml.safe_load(f)


# Conversion des jsonl en json

json_folder = action_on_folder.create_folder(
    "smart-enseigno/data/interim", "jsonl_to_json_sub_dict"
)
DATA = "smart-enseigno/data/raw/educlever-p2ia-v2r2265"

for file in os.listdir(DATA):
    # print(file)
    path_file = os.path.join(DATA, file)
    # print(path_file)
    convert_files.convert_jsonl_to_json(path_file, json_folder)

# Récupération des sous-dictionnaires

sub_dict_folder = action_on_folder.create_folder(
    "smart-enseigno/data/interim", "json_with_simple_sub_dict"
)
for file in os.listdir("smart-enseigno/data/interim/jsonl_to_json_sub_dict"):
    # print(file)
    path_file = os.path.join("smart-enseigno/data/interim/jsonl_to_json_sub_dict", file)
    sub_dict_json.get_sub_dict_from_json(path_file, sub_dict_folder)

# Conversion des nouveaux json en CSV

csv_folder = action_on_folder.create_folder(
    "smart-enseigno/data/interim", "csv_with_simple_sub_dict"
)
for file in os.listdir("smart-enseigno/data/interim/json_with_simple_sub_dict"):
    # print(file)
    path_file = os.path.join("smart-enseigno/data/interim/json_with_simple_sub_dict", file)
    convert_files.convert_json_to_csv(path_file, csv_folder)

# Visualisation des données

for file in os.listdir("smart-enseigno/data/interim/csv_with_simple_sub_dict"):
    # print(file)
    path_file = os.path.join("smart-enseigno/data/interim/csv_with_simple_sub_dict", file)
    # print(path_file)
    dashboard.analysis(
        path_file,
        "smart-enseigno/reports",
        "smart-enseigno/reports/sub_dict",
        folder_name="sub_dict",
    )


# print(time.time())
# print(start)
# print(time.ctime()[11:19])
# print((time.ctime(start))[11:19])
print(
    time.strftime("%H:%M:%S", time.gmtime(time.time() - start))
)  # Temps d'exexcution du script
