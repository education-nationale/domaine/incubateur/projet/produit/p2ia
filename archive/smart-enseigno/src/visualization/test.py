"""
Visualisation des données bruts de smart-enseigno
"""

import os
import time
import yaml
import pandas as pd
from src.visualization import dashboard
from src.features import convert_files, deep_sub_dict_json, action_on_folder

start = time.time()  # Heure de début de script

# with open("config\config.yaml\path.yaml") as f:
#         path = yaml.safe_load(f)


# Conversion des jsonl en json

json_folder = action_on_folder.create_folder(
    "smart-enseigno/data/external", "jsonl_to_json_deep_sub_dict"
)
DATA = "smart-enseigno/data/test"

for file in os.listdir(DATA):
    # print(file)
    path_file = os.path.join(DATA, file)
    # print(path_file)
    convert_files.convert_jsonl_to_json(path_file, json_folder)

# Récupération des sous-dictionnaires

sub_dict_folder = action_on_folder.create_folder(
    "smart-enseigno/data/external", "json_with_deep_sub_dict"
)
for file in os.listdir("smart-enseigno/data/external/jsonl_to_json_deep_sub_dict"):
    # print(file)
    path_file = os.path.join("smart-enseigno/data/external/jsonl_to_json_deep_sub_dict", file)
    deep_sub_dict_json.get_sub_dict_from_json(path_file, sub_dict_folder)

# Conversion des nouveaux json en CSV

csv_folder = action_on_folder.create_folder(
    "smart-enseigno/data/external", "csv_with_deep_sub_dict"
)
for file in os.listdir("smart-enseigno/data/external/json_with_deep_sub_dict"):
    print(file)
    path_file = os.path.join("smart-enseigno/data/external/json_with_deep_sub_dict", file)
    convert_files.convert_json_to_csv(path_file, csv_folder)

# Suppresions des colonnes inutiles dans les csv

final_csv = action_on_folder.create_folder(
"smart-enseigno/data/external", "reduce_csv"
)


for file in os.listdir("smart-enseigno/data/external/csv_with_deep_sub_dict"):
    
    chunks=[] #Liste des datatframes fractionnés
    print(file)
    path_file = os.path.join("smart-enseigno/data/external/csv_with_deep_sub_dict", file)
    if path_file.endswith(".csv"):
        data = pd.read_csv(path_file, sep=",", chunksize= 60000) #Fractionnement

    elif path_file.endswith(".xlsx") or path_file.endswith(".xls"):
        data = pd.read_excel(path_file)
        
    for chunk in data:
        #print(chunk)

        for column in chunk.columns:
            missing_values_total = chunk[column].isna().sum().sum()
            
            percentage_of_missing_values = (missing_values_total / chunk[column].shape[0] * 100)
            
            if percentage_of_missing_values>99 :
                # print(column)
                # print(percentage_of_missing_values)
                del chunk[column]
        chunks.append(chunk)
        
    data= pd.concat(chunks)


    for column in data.columns:
        missing_values_total = data[column].isna().sum().sum()
        
        percentage_of_missing_values = (missing_values_total / data[column].shape[0] * 100)
        
        if percentage_of_missing_values>99 :
            # print(column)
            # print(percentage_of_missing_values)
            del data[column]
    
    with open(f"{final_csv}/{file}", "w", encoding="utf-8") as csv_file:
        data.to_csv(path_or_buf=csv_file, index=False, lineterminator="")


# Visualisation des données

for file in os.listdir("smart-enseigno/data/external/reduce_csv"):
    # print(file)
    path_file = os.path.join("smart-enseigno/data/external/reduce_csv", file)
    # print(path_file)
    dashboard.analysis(
        path_file,
        "smart-enseigno/reports",
        "smart-enseigno/reports/test",
        folder_name="test",
    )


# print(time.time())
# print(start)
# print(time.ctime()[11:19])
# print((time.ctime(start))[11:19])
print(
    time.strftime("%H:%M:%S", time.gmtime(time.time() - start))
)  # Temps d'exexcution du script
