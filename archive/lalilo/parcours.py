import pandas as pd
from datetime import datetime
from constants import school_date_start

pd.set_option('display.max_columns', None)
df = pd.read_csv(
    'lalilo/data/extract_lalilo_data_April_2024/data/learning'
    '/all_student_traces.csv', sep=',', chunksize=10000
).get_chunk()

output_data = {
    "nom_outil": [],
    "id_elev": [],
    "parcours": [],
    "nb_activites": [],
    "nb_activites_dup": [],
    "nb_competences": [],
    "nb_comp_acquises": [],
    "lst_competences": [],
    "annee_scolaire": [],
    "dt_debut": [],
    "dt_fin": [],
    "nb_verbes": None
}

lst_competences = list(df["LEARNING_OBJECTIVE_ID"].unique())


def get_school_year(date_str: str) -> str:
    formats = ['%Y-%m-%dT%H:%M:%S.%fZ', '%Y-%m-%dT%H:%M:%SZ']
    for fmt in formats:
        try:
            date = datetime.strptime(date_str, fmt)
            break
        except ValueError:
            pass
    else:
        raise ValueError(
            f"Time data {date_str} does not match any known format"
            )

    school_date_start_datetime = datetime.strptime(school_date_start, '%d-%m')
    year = date.year
    if date >= school_date_start_datetime:
        school_year = f"{year}-{year + 1}"
    else:
        school_year = f"{year - 1}-{year}"
    return school_year


for student_id in df["STUDENT_ID"].unique():
    df_stud = df[df["STUDENT_ID"] == student_id]
    nb_activites = len(df_stud)
    nb_activites_dup = df_stud["LEARNING_OBJECTIVE_ID"].duplicated().sum()
    nb_competences = len(df_stud["LEARNING_OBJECTIVE_ID"].unique())
    nb_comp_acquises = len(df_stud[df_stud["SUCCESS_RATE"] == 1.0])
    dt_debut = df_stud["CREATED_AT"].min()
    dt_fin = df_stud["CREATED_AT"].max()

    annee_scolaire = get_school_year(dt_debut)

    output_data["nom_outil"].append("lalilo")
    output_data["id_elev"].append(student_id)
    output_data["parcours"].append(
        df_stud["LEARNING_OBJECTIVE_ID"].unique())
    output_data["nb_activites"].append(nb_activites)
    output_data["nb_activites_dup"].append(nb_activites_dup)
    output_data["nb_competences"].append(nb_competences)
    output_data["nb_comp_acquises"].append(nb_comp_acquises)
    output_data["lst_competences"].append(lst_competences)
    output_data["annee_scolaire"].append(annee_scolaire)
    output_data["dt_debut"].append(dt_debut)
    output_data["dt_fin"].append(dt_fin)

output_df = pd.DataFrame(output_data)

output_df.to_csv("./lalilo/output.csv", index=False)
