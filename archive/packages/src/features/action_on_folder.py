"""
Action sur les dossier :
* create_folder -> Création de dosssier stockage
"""

import os


def create_folder(path, folder_name):
    """
    Crée un dossier pour stocker les fichiers

    Args:
        - path (str): Chemin du dossier à créer/existant
        - folder_name (str): Nom du dossier

    Returns:
        total_path (str) : Chemin du dossier

    Raises :
        FileExistsError : Le dossier existe dèjà

    """
    total_path = f"{path}/{folder_name}"

    try:

        os.mkdir(total_path)
        print(f"Le dossier {folder_name} a été créé avec succès.")
        return total_path

    except FileExistsError:
        print(f"Le dossier {folder_name} existe déjà")
        return total_path

    except OSError as erreur:
        print(f"Erreur lors de la création du dossier {folder_name}: {erreur}")
        return None


# def folder_navigation(folder):
#     """
#     Explore les fichiers d'un dossier

#     Args:
#         - folder (str): Dossier à explorer

#     Returns:
#         files_list (list) : Liste des fichiers du dossier

#     """

#     files_list= [] #Liste fichiers

#     for file in os.listdir(folder):
#         #print(file)
#         path_file = os.path.join(folder, file)
#         #print(path_file)
#         files_list.append(path_file)

#     return files_list
