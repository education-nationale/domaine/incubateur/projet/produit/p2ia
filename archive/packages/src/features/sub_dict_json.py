"""Récupération des données contenues dans les sous-dictionaires dans un JSON"""

import os
import json


def work_in_sub_dict(test):
    """Récupère un dictionnaire et y récupére tout les sous-dictionnaires à l'intérieur

    Args:
        test (dict): Un dictionnaire

    Return:
        test(dictionaire): Le dictionnaire avec l'ensemble du parsing
        des sous-dictionnaires"""

    # print(test)

    del_keys = []  # Clés à supprimer
    new_dict = {}  # Tout les sous dictionnaires

    for keys in test.keys():
        # print(keys, test[keys], type(test[keys]))
        if isinstance(test[keys], dict):
            del_keys.append(keys)
            sub_dict = test[keys]

            # Liste des nouvelles clés associées à leurs nouveaux noms
            new_name = {
                old_name: f"{del_keys[-1]}/{old_name}" for old_name in sub_dict.keys()
            }
            old_name = list(sub_dict.keys())
            # print(new_name)
            for old_keys in old_name:
                sub_dict[new_name[old_keys]] = sub_dict.pop(old_keys)

            # print(del_keys)
            # print(sub_dict)
            new_dict.update(sub_dict)
            # print(new_dict)

    test.update(new_dict)
    for keys in del_keys:
        del test[keys]


    return test


def get_sub_dict_from_json(json_file, storage_folder):
    """
    Récupére les sous dictionnaires dans les json et les stockes dans un json

    Args:
        json_file (str) : Chemin du fichier à convertir.
        storage_folder (str) : Chemin du fichier de stockage.
    """
    sub_dict_list = []  # On stocke les sous_dictionnaires

    with open(json_file, "r", encoding="utf-8") as file_json:

        json_data = json.load(file_json)
        # print (json_data,len(json_data))
        for json_dict in json_data:
            test = json_dict.copy()

            sub_dict_list.append(work_in_sub_dict(test))

    # Création du nouveau Json
    json_name = os.path.basename(json_file)
    json_name = os.path.splitext(json_name)[0]
    json_name = f"{json_name}_simple_sub_dict"

    # Nom du fichier, on garde le nom sans le .json

    json_path = f"{storage_folder}/{json_name}.json"

    # Stocke les sous dictionnaires dans un json
    with open(json_path, "w", encoding="utf-8") as new_json:

        json.dump(sub_dict_list, new_json, indent=2)
