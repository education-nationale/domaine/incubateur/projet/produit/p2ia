"""
Conversion type de fichiers :

* JSON to CSV -> convert_json_to_csv
* CSV to JSON -> convert_csv_to_json
* JSONL to JSON -> convert_jsonl_to_json
"""

import os
import json
import csv
import pandas as pd

########### Conversion des json en csv #####################


def convert_json_to_csv(json_file, csv_path):
    """
    Crée un fichier csv à partir du fichier json

    Args:
        json_file (str) : Chemin du fichier à convertir.
        csv_path (str) : Chemin de dépot du fichier csv
    """

    # Ouvrir le fichier JSON en lecture
    with open(json_file, "r", encoding="utf-8") as file_json:
        # Charger les données JSON
        data = json.load(file_json)

    # Créer un DataFrame à partir des données JSON
    df = pd.DataFrame(data)

    csv_name = os.path.basename(json_file)
    csv_name = os.path.splitext(csv_name)[0]
    # Nom du fichier, on garde le nom sans le .json

    new_csv = f"{csv_path}/{csv_name}.csv"

    # Écrire le DataFrame dans un fichier CSV
    df.to_csv(new_csv, index=False)


################ Conversion de csv en json #######################


def convert_csv_to_json(csv_file, json_path):
    """
    Crée un fichier json à partir du fichier csv

    Args:
        csv_file (str) : Chemin du fichier à convertir.
        json_path (str) : Chemin de dépot du fichier json
    """

    # Ouvrir le fichier CSV en mode lecture
    with open(csv_file, "r", encoding="utf-8") as csv_input:
        # Lire le contenu du fichier CSV
        csv_data = csv.DictReader(csv_input)

        # Convertir les données en une liste de dictionnaires
        data_list = list(csv_data)

    json_name = os.path.basename(csv_file)
    json_name = os.path.splitext(json_name)[0]
    # Nom du fichier, on garde le nom sans le .csv

    new_json = f"{json_path}/{json_name}.json"

    # Décodage des objets sous format json au format python
    for entry in data_list:
        #print(entry)
        for key in entry.keys():
            # print(entry.keys())
            # print(key)
            # print(entry[key])
            try:
                #print(entry[key],type(entry[key]),"before load")
                entry[key] = json.loads(entry[key]) 
                #print(entry[key],type(entry[key]),"load")
            except(json.JSONDecodeError):
                #print(entry[key],type(entry[key]),"no load")
                pass

            

    with open(new_json, "w", encoding="utf-8") as json_file:
        # Utiliser ensure_ascii=False lors de la conversion en chaîne JSON
        json.dump(data_list, json_file, ensure_ascii=False, indent=4)


########### Conversion des jsonl en json #######################


def convert_jsonl_to_json(file_jsonl, json_path):
    """
    Crée un fichier json à partir du fichier jsonl correspondant

    Args:
        file_jsonl (str) : Chemin du fichier à convertir.
        json_path (str) : Chemin de dépot du fichier json

    """

    with open(file_jsonl, "r", encoding="utf-8") as jsonl_file:

        # Convertit les lignes du fichier JSONL de type JSON en dict
        json_line = [json.loads(line.strip()) for line in jsonl_file]
        # print(json_line,type(json_line))

        # print(json_line[0],type(json_line[0]), json_line[0].keys())
        test = json_line[0].copy()
        del_keys = []  # Clés à supprimer
        new_dict = {}  # Tout les sous dictionnaires
        for keys in test.keys():
            # print(keys, test[keys], type(test[keys]))
            if isinstance(test[keys], dict):
                sub_dict = test[keys]
                del_keys.append(keys)
                # print(sub_dict)
                # print(del_keys)
                new_dict.update(sub_dict)

        test.update(new_dict)
        for keys in del_keys:
            del test[keys]

        # print(test)

    # Concaténer les objets JSON en un seul
    json_file = json.dumps(json_line, indent=2)

    json_name = os.path.basename(file_jsonl)
    json_name = os.path.splitext(json_name)[0]

    # Nom du fichier, on garde le nom sans le .jsonl

    new_json = f"{json_path}/{json_name}.json"

    with open(new_json, "w", encoding="utf-8") as json_name:
        json_name.write(json_file)
