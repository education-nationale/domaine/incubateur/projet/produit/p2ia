"""Conversion des csv/xls en json puis en documentation pdf"""

import os
import os.path
import re
import random
import json
from datetime import date, datetime
from scipy import stats
import pandas as pd
import numpy as np
from reportlab.lib.pagesizes import letter
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer
from src.features import action_on_folder


def doc_json(file, sep=",", id=""):
    """
    Convertit un fichier CSV ou Excel
    en un format JSON contenant des informations statistiques sur les variables.

    Args:
        file (str): Chemin du fichier à convertir.
        sep (str, optional): Délimiteur utilisé pour les fichiers CSV. Par défaut, ','.
        ID (str or list, optional): Colonne(s) utilisée(s) comme identifiant(s). Par défaut, ''.

    Returns:
        dict: Un dictionnaire JSON contenant les informations statistiques
        sur les variables du fichier.

    Raises:
        TypeError: Si le fichier n'est pas au format CSV ou Excel.

    """
    # --------------------------------Ouverture du fichier-------------------------------
    if file.endswith(".csv"):
        data = pd.read_csv(file, sep=sep)
    elif file.endswith(".xlsx") or file.endswith(".xls"):
        data = pd.read_excel(file)
    else:
        return "Erreur : Le fichier doit être au format CSV ou Excel."
    # Récupération du fichier
    # --------------------------------------------------------------------------------------------

    # --------------------------------Nom et valeurs principales du fichier-----------------------
    file_name = os.path.basename(file)
    current_date = date.today().strftime("%d-%m-%Y")
    # Nom et date d'ouverture du fichier

    types = data.dtypes
    columns = data.columns
    missing_values = data.isna().sum()
    len_data = len(data)
    number_of_data = (len_data * len(columns),)
    missing_values_total = data.isna().sum().sum()
    percentage_of_missing_values = (missing_values_total / number_of_data * 100).round(
        2
    )
    # type des colonnes, lignes et valeurs manquantes

    creation_timestamp = os.path.getctime(file)
    creation_date = datetime.fromtimestamp(creation_timestamp)
    formatted_date = creation_date.strftime("%d-%m-%Y %H:%M:%S")
    # -------------------------------------------------------------------------------------------

    # --------------------------------Mise en ébauche des différentes colonnes-------------------
    variables = {}
    for column, data_type in zip(columns, types):
        variables[column] = {
            "type": str(data_type),
            "missing_values": int(missing_values[column]),
        }
    # valeurs classiques (type et nombre de valeurs manquantes)
    # ------------------------------------------------------------------------------------------

    # --------------------------------Colonne de type 'object'-------------------------------------
    object_columns = [col for col, dtype in types.items() if dtype == "object"]
    # On récupère les colonnes de type object

    for column in object_columns:
        column_data = data[column]

        value_counts = column_data.value_counts(normalize=True)
        # print(value_counts)
        # print(len(value_counts.index),len(value_counts.index.tolist()))
        top_values = value_counts[value_counts > 0.1]
        top10 = top_values.index.tolist()
        top10_percentages = (top_values * 100).round(2).tolist()
        unique_values = len(column_data.value_counts())

        if len(value_counts.index) != 0:

            highest_app = max(column_data.value_counts())
            nbr_hgh_app = sum(column_data.value_counts() == highest_app)

            if len_data - int(missing_values[column]) == unique_values:
                value_highest_app = "Tous les objets sont uniques"
            else:
                value_highest_app = value_counts.index.tolist()[0:nbr_hgh_app]

        else:

            highest_app = "Pas de valeurs"
            nbr_hgh_app = "Pas de valeurs"
            value_highest_app = "Pas de valeurs"

        # Valeurs à ajouter dans les variables objets

        top_values = value_counts[value_counts > 0.1]
        top10 = top_values.index.tolist()
        top10_percentages = (top_values * 100).round(2).tolist()
        # On calcule les pourcentages des valeurs les plus présentes dans chaque colonne
        # de type 'object' en affichant que celle de plus de 10%
        variables[column] = {
            "type": str(types[column]),
            "missing values": int(missing_values[column]),
            "missing values percent column": round(
                (int(missing_values[column]) / len_data) * 100, 2
            ),
            "unique value": unique_values,
            "value highest appearance": value_highest_app,
            "highest appearance": highest_app,
            "nbr of highest app": nbr_hgh_app,
            ">10% appearance": dict(zip(top10, top10_percentages)),
        }
    # ----------------------------------------------------------------------------------------

    # --------------------------------Colonne de type date-----------------------------------------
    date_columns = [col for col, dtype in types.items() if dtype == "datetime64[ns]"]
    # On récupère les colonnes de type date

    for column in data.columns:
        if column not in date_columns:
            # Vérification des valeurs de la colonne
            for value in data[column]:
                if re.match(r"(\d{4}-\d{2}-\d{2})|(\d{2}-\d{2}-\d{4})", str(value)):
                    # Vérification du format de date
                    date_columns.append(column)

    date_columns = list(set(date_columns))
    # Supprimer les doublons

    for column in date_columns:
        column_data = data[column].dropna()
        date_formats = set()

        for date_value in column_data:
            if type(date_value) == "datetime64[ns]":
                date_str = date_value.strftime("%d-%m-%Y")
                # Conversion en chaine de caractère

            else:
                date_str = date_value

            try:
                datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S")
                date_formats.add("ISO 8601")
            except ValueError:
                pass
            # Format ISO

            try:
                datetime.strptime(date_str, "%d-%m-%Y")
                date_formats.add("European")
            except ValueError:
                pass

            try:
                datetime.strptime(date_str, "%d-%m-%Y %H:%M:%S")
                date_formats.add("European")
            except ValueError:
                pass
            # Formats européen

            try:
                datetime.strptime(date_str, "%m-%d-%Y")
                date_formats.add("American")
            except ValueError:
                pass

            try:
                datetime.strptime(date_str, "%m-%d-%Y %H:%M:%S")
                date_formats.add("American")
            except ValueError:
                pass
            # Formats américain

            if "European" in date_formats:
                selected_format = "European"
            elif "American" in date_formats:
                selected_format = "American"
            elif "ISO 8601" in date_formats:
                selected_format = "ISO 8601"
            else:
                selected_format = "Unknown"
            # Récupération du format

        if str(types[column]) == "datetime64[ns]":
            variables[column] = {
                "type": str(types[column]),
                "real_type": str(types[column]),
                "extremum": {
                    "earliest_date": (np.min(column_data).strftime("%d-%m-%Y")),
                    "latest_date": np.max(column_data).strftime("%d-%m-%Y"),
                },
                "missing values": int(missing_values[column]),
                "missing values percent column": round(
                    (int(missing_values[column]) / len_data) * 100, 2
                ),
                "date format": selected_format,
            }
        else:
            variables[column] = {
                "type": "datetime64[ns]",
                "real_type": str(types[column]),
                "extremum": {
                    "earliest_date": np.min(column_data),
                    "latest_date": np.max(column_data),
                },
                "missing values percent column": round(
                    (int(missing_values[column]) / len_data) * 100, 2
                ),
                "missing values": int(missing_values[column]),
                "date format": selected_format,
            }

    # On remplit avec les valeurs obtenues
    # --------------------------------------------------------------------------------------------

    # --------------------------------Colonne de type 'année'-------------------------------------
    # print(types)
    # print(types.items)
    year_columns = [
        col
        for col, dtype in types.items()
        if dtype == "int64"
        and (col.lower() == "annee" or col.lower() == "année" or col.lower() == "year")
    ]
    # print(year_columns)

    # # Vérifier les années dans chaque colonne

    # for column in data.columns:
    #     if column not in year_columns:
    #         # Vérification des valeurs de la colonne
    #         for value in data[column]:

    #             if isinstance(value, (int, float, str)):
    #                 value_str = str(value)
    #             # Conversion en chaîne de caractères

    #             if re.match(r"^\d{4}$", value_str):
    #                 # Vérification du format de l'année

    #                 year_columns.append(column)

    # year_columns = list(set(year_columns))
    # # Supprimer les doublons

    # for column in year_columns:
    #     column_data = data[column]
    #     variables[column] = {
    #         "real_type": str(types[column]),
    #         "type": str("year"),
    #         "extremum": {
    #             "earliest_year": np.min(column_data),
    #             "latest_year": np.max(column_data),
    #         },
    #         "missing values": int(missing_values[column]),
    #         "missing values percent column": round(
    #             (int(missing_values[column]) / len_data) * 100, 2
    #         ),
    #     }
    # # On remplit avec les valeurs extrêmes
    # ---------------------------------------------------------------------------------------------

    # --------------------------------Colonne de type 'binary'--------------------------------
    binary_columns = [col for col, dtype in types.items() if dtype == "bool"]
    # On récupère les colonnes de type date

    for column in data.columns:
        if column not in binary_columns:
            # Initialisation
            unique_values = data[column].dropna().unique()

            if len(unique_values) == 2:
                binary_columns.append(column)
        # On écarte les valeurs manquantes
        # On regarde quels colonnes possède exactement deux valeurs uniques

    for column in binary_columns:
        column_data = data[column]
        value_counts = column_data.value_counts(normalize=True)
        top = value_counts.index.tolist()
        top_percentages = (value_counts * 100).round(2).tolist()
        variables[column] = {
            "type": str("binary"),
            "real_type": str(types[column]),
            "missing values": int(missing_values[column]),
            "missing values percent column": round(
                (int(missing_values[column]) / len_data) * 100, 2
            ),
            "appearance": dict(zip(top, top_percentages)),
        }
        # On remplit avec les pourcentages de présence de chacune des valeurs
    # ------------------------------------------------------------------------------

    # --------------------------------Colonne de type 'catégoriel'--------------------------------
    categorical_columns = []

    # Initialisation

    for column in data.columns:
        if column not in binary_columns:
            unique_values = data[column].dropna().unique()
            value_counts = data[column].value_counts(normalize=True, dropna=True)
            # print(len(value_counts[value_counts > 0.1]),len(unique_values),
            #       len(value_counts[value_counts > 0.1])/len(unique_values))
            if (
                len(unique_values) != 1
                and len(unique_values) != 0
                and (
                    0.1
                    <= len(value_counts[value_counts > 0.1]) / len(unique_values)
                    <= 1
                )
            ):
                categorical_columns.append(column)
            # On écarte les valeurs manquantes
            # On conserve les colonnes ayant une grande proportion de valeurs uniques

    for column in categorical_columns:
        column_data = data[column]
        value_counts = column_data.value_counts(normalize=True)
        top = value_counts.index.tolist()
        top_percentages = (value_counts * 100).round(2).tolist()
        unique_values = len(column_data.dropna().unique())
        highest_app = max(column_data.value_counts())
        nbr_hgh_app = sum(column_data.value_counts() == highest_app)
        value_highest_app = value_counts.index.tolist()[0:nbr_hgh_app]

        # Valeurs à ajouter dans les variables catégorielles

        variables[column] = {
            "real_type": str(types[column]),
            "type": str("catégoriel"),
            "missing values": int(missing_values[column]),
            "missing values percent column": round(
                (int(missing_values[column]) / len_data) * 100, 2
            ),
            "unique value": unique_values,
            "value highest appearance": value_highest_app,
            "highest appearance": highest_app,
            "nbr of highest app": nbr_hgh_app,
            "appearance": dict(zip(top, top_percentages)),
        }
        # On remplit avec les pourcentages de présence de chacune des valeurs
    # ------------------------------------------------------------------------------

    # --------------------------------Colonne de type ID--------------------------------
    id_columns = []
    # Initialisation de la colonne

    for column in data.columns:
        values = data[column].dropna()
        values = values.astype(str)
        # Conversion des valeurs en chaînes de caractères
        # if len(set(map(len, values))) == 1 and len(set(values))!=1:
        #     print(set(values),len(set(values)))
        if (
            column not in year_columns
            and column not in binary_columns
            and column not in categorical_columns
            and column not in date_columns
            and len(set(map(len, values))) == 1  # Tout les objets de même longeurs
            and len(set(values)) != 1  # Pas de colonne valeur fixe
        ) or ("id" in column.lower()):
            id_columns.append(column)

    for column in id_columns:
        # print(column, type(column))
        column_data = data[column]
        value_counts = column_data.value_counts(normalize=True)
        # print(value_counts)
        # print(len(value_counts.index),len(value_counts.index.tolist()))
        top_values = value_counts[value_counts > 0.1]
        top10 = top_values.index.tolist()
        top10_percentages = (top_values * 100).round(2).tolist()
        unique_values = len(column_data.value_counts())

        if len(value_counts.index) != 0:

            highest_app = max(column_data.value_counts())
            nbr_hgh_app = sum(column_data.value_counts() == highest_app)

            if len_data - int(missing_values[column])  == unique_values:
                value_highest_app = "Tous les indentifiants sont uniques"
            else:
                value_highest_app = value_counts.index.tolist()[0:nbr_hgh_app]

        else:

            highest_app = "Pas de valeurs"
            nbr_hgh_app = "Pas de valeurs"
            value_highest_app = "Pas de valeurs"

        # Valeurs à ajouter dans les variables identifiants

        variables[column] = {
            "real_type": str(types[column]),
            "type": str("Identifiant"),
            "missing values": int(missing_values[column]),
            "missing values percent column": round(
                (int(missing_values[column]) / len_data) * 100, 2
            ),
            "unique value": unique_values,
            "value highest appearance": value_highest_app,
            "highest appearance": highest_app,
            "nbr of highest app": nbr_hgh_app,
            ">10% appearance": dict(zip(top10, top10_percentages)),
        }
        # On remplit avec les pourcentages de présence de chacune des valeurs
    # ---------------------------------------------------------------------------------

    # --------------------------------Colonne de type ID-------------------------------
    try:
        # On teste si ID n'est pas vide
        if id:
            id_colonne = data[id]

            unique_values = len(id_colonne.value_counts())
            highest_app = max(id_colonne.value_counts())
            nbr_hgh_app = sum(id_colonne.value_counts() == highest_app)
            value_highest_app = value_counts.index.tolist()[0:nbr_hgh_app]
            # valeur unique et nombre d'apparitions

            value_counts = id_colonne.value_counts(normalize=True)
            top_values = value_counts[value_counts > 0.1]
            top10 = top_values.index.tolist()
            top10_percentages = (top_values * 100).round(2).tolist()
            # Valeur avec le plus de présence et son pourcentage

            variables[id] = {
                "type": str("ID"),
                "missing values": int(missing_values[id]),
                "missing values percent column": round(
                    (int(missing_values[column]) / len_data) * 100, 2
                ),
                "unique value": unique_values,
                "value highest appearance": value_highest_app,
                "highest appearance": highest_app,
                "nbr of highest app": nbr_hgh_app,
                ">10% appearance": dict(zip(top10, top10_percentages)),
            }
    except KeyError:
        pass
    # ---------------------------------------------------------

    # --------------------------------Colonnes de types 'float/int'-----------------------------
    float_int_columns = []
    # Initialisation de la colonne

    for column in data.columns:
        if (
            column not in object_columns
            and column not in date_columns
            and column not in year_columns
            and column not in binary_columns
            and column not in categorical_columns
            and column not in id_columns
        ):
            float_int_columns.append(column)

    for column in float_int_columns:

        column_data = data[column]
        column_data = column_data.apply(pd.to_numeric, errors="raise")
        # Conversion en valeurs numerique

        if len(data[column] < 3000):
            sample_size = len(data[column])
        else:
            sample_size = 3000
        # Taille du sample pour le test statistique

        sample = random.sample(column_data.tolist(), sample_size)
        shapiro_stat, shapiro_pvalue = stats.shapiro(sample)
        # Test de shapiro
        q1 = np.percentile(column_data, 25)
        q3 = np.percentile(column_data, 75)
        iqr = q3 - q1
        lower_bound = q1 - 1.5 * iqr
        upper_bound = q3 + 1.5 * iqr
        outliers = column_data[
            (column_data < lower_bound) | (column_data > upper_bound)
        ]
        # Calcul des valeurs aberrantes

        # On fait attention car si la valeur du nombre de valeurs manquantes total est null
        variables[column] = {
            "type": str(types[column]),
            "statistics": {
                "mean": round(np.mean(column_data), 2),
                "std": round(np.std(column_data), 2),
                "min": round(np.min(column_data), 2),
                "max": round(np.max(column_data), 2),
                "q1": round(q1, 2),
                "q3": round(q3, 2),
                "shapiro_statistic": round(shapiro_stat, 2),
                "shapiro_pvalue": round(shapiro_pvalue, 2),
                "outliers": len(outliers.tolist()),
            },
            "missing values": int(missing_values[column]),
            "missing values percent column": round(
                (int(missing_values[column]) / len_data) * 100, 2
            ),
        }
        # On remplit le dictionnaire avec les valeurs
    # ---------------------------------------------------------------------------------------------
    # --------------------------------Mise en forme des résultats-----------------------------------
    json_resultat = {
        "file_properties": {
            "file_name": file_name,
            "date": current_date,
            "Number of lines": len_data,
            "Number of columns": len(columns),
            "Name of columns": columns.tolist(),
            "Number of data": number_of_data,
            "Missing values": missing_values_total,
            "Percentage of missing values": f"{percentage_of_missing_values} %",
            "Creation date": formatted_date,
        },
        "variables": variables,
    }
    # ----------------------------------------------------
    return json_resultat


def format_json(json_data):
    """
    Formate le contenu JSON pour une meilleure lisibilité.

    Arguments :
    - json_data : dict : Les données JSON à formater.

    Retourne :
    - str : Le contenu JSON formaté.
    """
    indent = 4
    sorted_json = json.dumps(
        json_data,
        indent=indent,
        sort_keys=True,
        ensure_ascii=False,
        default=convert_to_builtin_type,
    )

    formatted_json = ""

    level = 0
    for char in sorted_json:
        if char == "{":
            formatted_json += char + "\n" + " " * (indent * (level + 1))
            level += 1
        elif char == "}":
            formatted_json += "\n" + " " * (indent * (level - 1)) + char
            level -= 1
        elif char == ",":
            formatted_json += char + "\n" + " " * (indent * level)
        else:
            formatted_json += char

    return formatted_json


def convert_to_builtin_type(obj):
    """
    Convertit les types d'objets personnalisés en types JSON sérialisables.

    Arguments :
    - obj : object : L'objet à convertir.

    Retourne :
    - object : L'objet converti.

    Raises :
    - TypeError : Si l'objet n'est pas sérialisable en JSON.
    """
    if isinstance(obj, np.int64):
        return int(obj)
    raise TypeError(f"Object of type '{type(obj).__name__}' is not JSON serializable")


class PDF:
    """
    Classe personnalisée PDF
    """

    def __init__(self):
        self.story = []

    def header(self, canvas, doc):
        """
        Définit le contenu de l'en-tête du document PDF.
        """
        header_text = "Documentation de CSV/XLSX"
        canvas.drawString(doc.width / 2, doc.height + 100, header_text)
        self.footer(canvas, doc)  # Affiche page 1

    def footer(self, canvas, doc):
        """
        Définit le contenu du pied de page du document PDF.
        """
        page_num = canvas.getPageNumber()
        footer_text = f"Page {page_num}"
        canvas.drawString(doc.width / 2, 30, footer_text)

    def chapter_title(self, title):
        """
        Ajoute un titre de chapitre au document PDF.

        Arguments :
        - title : str : Le titre du chapitre.
        """
        style = getSampleStyleSheet()["Heading1"]
        self.story.append(Paragraph(title, style))
        self.story.append(Spacer(1, 12))

    def chapter_body(self, content):
        """
        Ajoute le contenu d'un chapitre au document PDF.

        Arguments :
        - content : str : Le contenu du chapitre.
        """
        style = getSampleStyleSheet()["BodyText"]
        self.story.append(Paragraph(content, style))
        self.story.append(Spacer(1, 12))


def create_pdf_from_json(json_data, output_file, file_name):
    """
    Crée un fichier PDF à partir des données JSON fournies.

    Arguments :
    - json_data : str : Les données JSON au format texte.
    - output_file : str : Le nom du fichier PDF de sortie.
    - file_name : str : Le nom du fichier d'origine.

    Cette fonction crée un fichier PDF à partir des données JSON fournies.

    Returns:
        .pdf: un pdf contenant les informations du json.
    """
    pdf = PDF()
    pdf.chapter_title(file_name)

    data = json_data

    pdf.chapter_title("File properties")
    pdf.chapter_body(str(data["file_properties"]))

    pdf.chapter_title("Variables")
    for key, value in data["variables"].items():
        pdf.chapter_body(f"{key} : {value}")

    doc = SimpleDocTemplate(output_file, pagesize=letter)
    doc.build(pdf.story, onFirstPage=pdf.header, onLaterPages=pdf.footer)


def main(file, folder_path, sep=",", folder_name="reports"):
    """
    Met en application les différentes fonctions établis précédemment.
    Et renvoie un fichier json du fichier passé en argument.
    On peut aussi préciser le séparateur dans le cas ou = ;

    Arguments :
    - file : chemin vers le fichier .csv/.xlsx.
    - folder_path : chemin du fichier stockage json exitant ou non
    - sep : le séparateur utilisé dans le fichier, par défaut ",".
    - folder_name (str): Nom du dossier existant ou non, par défaut "reports"

    Cette fonction crée un dictionnaire json à partir du fichier donné en argument.
    Crée le dossier stockage json et pdf.

    Returns:
        .json : un dictionnaire json contenant les informations du fichier.
    """
    file_name = os.path.basename(file)
    file_name = os.path.splitext(file_name)[0]
    # Nom du fichier, on garde le nom sans le .json

    result = doc_json(file, sep)

    principal_dir = action_on_folder.create_folder(folder_path, folder_name)
    output_dir = action_on_folder.create_folder(principal_dir, file_name)
    output_file = f"{output_dir}/{file_name}.pdf"
    create_pdf_from_json(result, output_file, file_name)

    with open(f"{output_dir}/{file_name}.json", "w", encoding="utf-8") as json_file:
        json.dump(
            result,
            json_file,
            sort_keys=True,
            ensure_ascii=False,
            default=convert_to_builtin_type,
        )
