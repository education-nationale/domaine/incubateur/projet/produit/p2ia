"""Conversion des Json en Dashboard"""

import os
import json
from bokeh.plotting import figure, show
from bokeh.io import output_file
from bokeh.models import (
    ColumnDataSource,
    Div,
    HoverTool,
)
from bokeh.layouts import column, layout

from src.visualization import json_doc


def binary_graph(data):
    """
    Prépare les graphiques de type binaires

    Arguments :
    - data : les données en format json.

    Retourne :
    - plots : Les graphes en barre binaires préparés.
    - title : Le titre de la catégorie.
    """
    binary_columns = []

    for var_column, var in data["variables"].items():
        if var["type"] == "binary":
            binary_columns.append(var_column)
    # Récupération des colonnes de type 'binary'

    plots = []
    # Initialisation des plots

    for col in binary_columns:

        values = [data["variables"][col]["appearance"]]
        binary_keys = list(values[0].keys())
        binary_values = list(values[0].values())
        # Récupération des labels et des values de chaque colonne

        x_range = binary_keys
        source = ColumnDataSource(data={"x": x_range, "value": binary_values})
        # Mise en place des données

        bin_graph = figure(x_range=x_range, height=400, width=600, title=f"{col}")
        bin_graph.title.align = "center"
        # Initialisation de la figure

        tooltips = [("Valeurs binaires", "@x"), ("Pourcentage", "@value")]
        hover_tool = HoverTool(tooltips=tooltips)
        bin_graph.add_tools(hover_tool)
        # Ajout de l'outil de survol avec les tooltips

        bin_graph.vbar(
            x="x",
            top="value",
            width=0.4,
            legend_label="Valeur",
            source=source,
            color="steelblue",
            alpha=0.8,
        )

        bin_graph.xgrid.grid_line_color = None
        # Style

        bin_graph.y_range.start = 0
        bin_graph.xaxis.axis_label = "Valeurs binaires"
        bin_graph.yaxis.axis_label = "Pourcentage"
        # Axes

        bin_graph.legend.label_text_font_size = "10pt"
        bin_graph.legend.location = "top_right"
        # Legend

        bin_graph.title.text_font_size = "14pt"
        # Style du titre

        plots.append(bin_graph)

    title = Div(
        text=""""<h1 style='text-align: center; color: #A60000; font-size: 24px;
        font-weight: bold;'>Graphique Binaire</h1>"""
    )
    # Titre de la section

    return plots, title


def binary_info(data):
    """
    Prépare les informations de type 'binary'

    Arguments :
    - data : les données en format json.

    Retourne :
    - div : Les informations des différentes colonnes.
    - title : Le titre de la catégorie.
    """
    binary_columns = []

    for var_column, var in data["variables"].items():
        if var["type"] == "binary":
            binary_columns.append(var_column)
    # Récupération des colonnes de type 'binary'

    divs = []

    for col in binary_columns:
        values = data["variables"][col]

        text = f"<b>Colonne :</b> {col}<br>"
        text += f"<b>Type :</b> {values['type']}<br>"
        text += f"<b>Type réel :</b> {values['real_type']}<br>"
        text += f"<b>Valeurs manquantes :</b> {values['missing values']}<br>"
        text += f"""<b>Pourcentage valeurs manquantes
        :</b> {values['missing values percent column']}%<br>"""
        text += f"""<b>Pourcentages des valeurs récurrentes
        :</b> {values['appearance']}<br>"""

        div = Div(text=text, width=400)
        divs.append(div)
        # Ajout des informations
    title = Div(
        text="""<h1 style='text-align: center; color: #000080; font-size: 24px;
        font-weight: bold;'>Information Binaires</h1>"""
    )
    return divs, title


def categorial_graph(data):
    """
    Prépare les graphiques de type binaires

    Arguments :
    - data : les données en format json.

    Retourne :
    - plots : Les graphes en barre binaires préparés.
    - title : Le titre de la catégorie.
    """
    categorial_columns = []

    for var_column, var in data["variables"].items():
        if var["type"] == "catégoriel":
            categorial_columns.append(var_column)
    # Récupération des colonnes de type 'catégoriel'

    plots = []
    # Initialisation des plots

    for col in categorial_columns:

        values = [data["variables"][col]["appearance"]]
        categorial_keys = list(values[0].keys())
        categorial_values = list(values[0].values())
        # Récupération des labels et des values de chaque colonne

        x_range = categorial_keys
        source = ColumnDataSource(data={"x": x_range, "value": categorial_values})
        # Mise en place des données

        cat_graph = figure(x_range=x_range, height=400, width=600, title=f"{col}")
        cat_graph.title.align = "center"
        # Initialisation de la figure

        tooltips = [("Valeurs catégorielles", "@x"), ("Pourcentage", "@value")]
        hover_tool = HoverTool(tooltips=tooltips)
        cat_graph.add_tools(hover_tool)
        # Ajout de l'outil de survol avec les tooltips

        cat_graph.vbar(
            x="x",
            top="value",
            width=0.4,
            legend_label="Valeur",
            source=source,
            color="steelblue",
            alpha=0.8,
        )

        cat_graph.xgrid.grid_line_color = None
        # Style

        cat_graph.y_range.start = 0
        cat_graph.xaxis.axis_label = "Valeurs catégorielles"
        cat_graph.yaxis.axis_label = "Pourcentage"
        # Axes

        cat_graph.legend.label_text_font_size = "10pt"
        cat_graph.legend.location = "top_right"
        # Legend

        cat_graph.title.text_font_size = "14pt"
        # Style du titre

        plots.append(cat_graph)

    title = Div(
        text=""""<h1 style='text-align: center; color: #A60000; font-size: 24px;
        font-weight: bold;'>Graphique Catégoriel</h1>"""
    )
    # Titre de la section

    return plots, title


def categorial_info(data):
    """
    Prépare les informations de type 'categorial'

    Arguments :
    - data : les données en format json.

    Retourne :
    - div : Les informations des différentes colonnes.
    - title : Le titre de la catégorie.
    """
    categorial_columns = []

    for var_column, var in data["variables"].items():
        if var["type"] == "catégoriel":
            categorial_columns.append(var_column)
    # Récupération des colonnes de type 'catégorial'

    divs = []

    for col in categorial_columns:
        values = data["variables"][col]

        text = f"<b>Colonne :</b> {col}<br>"
        text += f"<b>Type :</b> {values['type']}<br>"
        text += f"<b>Type réel :</b> {values['real_type']}<br>"
        text += f"<b>Valeurs uniques :</b> {values['unique value']}<br>"
        text += f"<b>Valeurs manquantes :</b> {values['missing values']}<br>"
        text += f"""<b>Pourcentage valeurs manquantes
        :</b> {values['missing values percent column']}%<br>"""
        text += f"<b>Valeurs les plus récurentes :</b> {values['value highest appearance']}<br>"
        text += f"<b>Nombre de valeurs les plus récurentes :</b> {values['nbr of highest app']}<br>"
        text += f"<b>Nombre de récurrence :</b> {values['highest appearance']}<br>"
        text += f"<b>Pourcentage de récurrence :</b> {values['appearance']}<br>"

        div = Div(text=text, width=400)
        divs.append(div)
        # Ajout des informations
    title = Div(
        text="""<h1 style='text-align: center; color: #000080; font-size: 24px;
        font-weight: bold;'>Informations Catégorielles</h1>"""
    )
    return divs, title


# def id_graph(data):
#     """
#     Prépare les graphiques de type 'identifiant'

#     Arguments :
#     - data : les données en format json.

#     Retourne :
#     - plots : Les graphes en barre binaires préparés.
#     - title : Le titre de la catégorie.
#     """
#     id_columns = []

#     for var_column, var in data["variables"].items():
#         if var["type"] == "Identifiant":
#             id_columns.append(var_column)
#     # Récupération des colonnes de type 'Identifiants'

#     plots = []

#     for col in id_columns:
#         values = [data["variables"][col][">10% appearance"]]
#         id_keys = list(values[0].keys())
#         id_values = list(values[0].values())
#         # Récupération des labels et des values de chaque colonne

#         x_range = id_keys
#         source = ColumnDataSource(data={"x": x_range, "value": id_values})
#         # Mise en place des données,
#          ColumnDataSource transforme le dictionnaire en dataframe pandas

#         graph = figure(x_range=x_range, height=400, width=600, title=f"{col}")
#         graph.title.align = "center"
#         # Initialisation de la figure

#         tooltips = [("Valeurs", "@x"), ("Pourcentage", "@value")]
#         hover_tool = HoverTool(tooltips=tooltips)
#         graph.add_tools(hover_tool)
#         # Ajout de l'outil de survol avec les tooltips

#         graph.vbar(
#             x="x",
#             top="value",
#             width=0.4,
#             legend_label="Valeur",
#             source=source,
#             color="steelblue",
#             alpha=0.8,
#         )
#         graph.xgrid.grid_line_color = None
#         # Style du graphe

#         graph.y_range.start = 0
#         graph.xaxis.axis_label = "Valeurs"
#         graph.yaxis.axis_label = "Pourcentage"
#         # Axes

#         graph.legend.label_text_font_size = "10pt"
#         graph.legend.location = "top_right"
#         # Legend

#         graph.title.text_font_size = "14pt"

#         plots.append(graph)
#         # Ajout des graphes

#     title = Div(
#         text="""<h1 style='text-align: center; color: #A60000; font-size: 24px;
#         font-weight: bold;'>Graphique identifiants</h1>"""
#     )
#     return plots, title


def id_info(data):
    """
    Prépare les informations de type 'identifiant'

    Arguments :
    - data : les données en format json.

    Retourne :
    - div : Les informations des différentes colonnes.
    - title : Le titre de la catégorie.
    """
    id_columns = []

    for var_column, var in data["variables"].items():
        if var["type"] == "Identifiant":
            id_columns.append(var_column)

    divs = []

    for col in id_columns:
        values = data["variables"][col]

        text = f"<b>Colonne :</b> {col}<br>"
        text += f"<b>Type :</b> {values['type']}<br>"
        text += f"<b>Type réel :</b> {values['real_type']}<br>"
        text += f"<b>Valeurs manquantes :</b> {values['missing values']}<br>"
        text += f"""<b>Pourcentage valeurs manquantes
        :</b> {values['missing values percent column']}%<br>"""
        text += f"<b>Valeurs uniques :</b> {values['unique value']}<br>"

        text += f"<b>Valeurs les plus récurentes :</b> {values['value highest appearance']}<br>"
        text += f"<b>Nombre de valeurs les plus récurentes :</b> {values['nbr of highest app']}<br>"
        text += f"<b>Nombre de récurrence :</b> {values['highest appearance']}<br>"
        text += f"<b>Récurrence >10%  :</b> {values['>10% appearance']}<br>"
        # Traitement des valeurs des colonnes de type date

        div = Div(text=text, width=400)
        divs.append(div)
        # Ajout des informations
    title = Div(
        text="""<h1 style='text-align: center; color: #000080; font-size: 24px;
        font-weight: bold;'>Information identifiants</h1>"""
    )
    return divs, title


def tableau(data):
    """
    Prépare les informations des données continus (float64/int64)

    Arguments :
    - data : les données en format json.

    Retourne :
    - divs : Le texte préparé avec les données de data.
    - title : Le titre de la catégorie.
    """
    float_cols = []

    for var_column, var in data["variables"].items():
        if var["type"] == "int64" or var["type"] == "float64":
            float_cols.append(var_column)
    # Récupération des colonnes

    divs = []

    for col in float_cols:
        values = data["variables"][col]

        text = f"<b>Colonne :</b> {col}<br>"
        text += f"<b>Type :</b> {values['type']}<br>"

        if "statistics" in values:
            statistics = values["statistics"]
            text += f"<b>Valeurs manquantes :</b> {values['missing values']}<br>"

            text += f"""<b>Pourcentage valeurs manquantes
            :</b> {values['missing values percent column']}%<br>"""

            text += f"<b>Valeur maximale :</b> {statistics['max']}<br>"
            text += f"<b>Valeur minimale :</b> {statistics['min']}<br>"
            text += f"<b>Valeur moyenne :</b> {statistics['mean']}<br>"
            text += f"<b>Ecart type :</b> {statistics['std']}<br>"
            text += (
                f"<b>Quartile :</b> q1={statistics['q1']}, q3={statistics['q3']}<br>"
            )
            text += f"<b>Valeurs aberrantes :</b> {statistics['outliers']}<br>"
            text += f"""<b>Test de Shapiro :</b> stat = {statistics['shapiro_statistic']},
            p-value = {statistics['shapiro_pvalue']}<br>"""

            # Informations pour les colonnes possédant des statistiques

        if "extremum" in values:
            extremum = values["extremum"]
            text += f"<b>Valeur minimale :</b> {extremum['earliest_year']}<br>"
            text += f"<b>Valeur maximale :</b> {extremum['latest_year']}<br>"
            # Informations pour les colonnes numériques de la forme 'annee'

        div = Div(text=text, width=400)
        divs.append(div)
        # Ajout des informations

    title = Div(
        text="""<h1 style='text-align: center; color: #000080; font-size: 24px;
        font-weight: bold;'>Informations numériques</h1>"""
    )
    return divs, title


# def year(data):
#     """
#     Prépare les informations des données continus (float64/int64)

#     Arguments :
#     - data : les données en format json.

#     Retourne :
#     - divs : Le texte préparé avec les données de data.
#     - title : Le titre de la catégorie.
#     """
#     year_cols = []

#     for var_column, var in data["variables"].items():
#         if var["type"] == "year":
#             year_cols.append(var_column)
#     # Récupération des colonnes

#     divs = []

#     for col in year_cols:
#         values = data["variables"][col]

#         text = f"<b>Colonne :</b> {col}<br>"
#         text += f"<b>Type :</b> {values['type']}<br>"
#         text += f"<b>Type réel :</b> {values['real_type']}<br>"
#         text += f"<b>Valeurs manquantes :</b> {values['missing values']}<br>"
#         text += f"""<b>Pourcentage valeurs manquantes
#         :</b> {values['missing values percent column']}%<br>"""
#         if "extremum" in values:
#             extremum = values["extremum"]
#             text += f"<b>Valeur minimale :</b> {extremum['earliest_year']}<br>"
#             text += f"<b>Valeur maximale :</b> {extremum['latest_year']}<br>"
#             # Informations pour les colonnes numériques de la forme 'annee'

#         div = Div(text=text, width=400)
#         divs.append(div)
#         # Ajout des informations

#     title = Div(
#         text="""<h1 style='text-align: center; color: #000080; font-size: 24px;
#         font-weight: bold;'>Informations années</h1>"""
#     )
#     return divs, title


def date_tab(data):
    """
    Prépare les informations des données de type date

    Arguments :
    - data : les données en format json.

    Retourne :
    - divs : Le texte préparé avec les données de data.
    - title : Le titre de la catégorie.
    """
    date_cols = []

    for var_column, var in data["variables"].items():
        if var["type"] == "datetime64[ns]":
            date_cols.append(var_column)
    # Récupération des colonnes de type date

    divs = []

    for col in date_cols:
        values = data["variables"][col]

        text = f"<b>Colonne :</b> {col}<br>"
        text += f"<b>Type :</b> {values['type']}<br>"
        text += f"<b>Type réel :</b> {values['real_type']}<br>"
        text += f"<b>Valeurs manquantes :</b> {values['missing values']}<br>"
        text += f"""<b>Pourcentage valeurs manquantes
        :</b> {values['missing values percent column']}%<br>"""
        text += f"<b>Format :</b> {values['date format']}<br>"

        extremum = values["extremum"]
        text += f"<b>Valeur minimale :</b> {extremum['earliest_date']}<br>"
        text += f"<b>Valeur maximale :</b> {extremum['latest_date']}<br>"
        # Traitement des valeurs des colonnes de type date

        div = Div(text=text, width=400)
        divs.append(div)
        # Ajout des informations

    title = Div(
        text="""<h1 style='text-align: center; color: #000080; font-size: 24px;
        font-weight: bold;'>Date</h1>"""
    )
    return divs, title


def object_tab(data):
    """
    Prépare les informations des données de type date

    Arguments :
    - data : les données en format json.

    Retourne :
    - divs : Le texte préparé avec les données de data.
    - title : Le titre de la catégorie.
    """
    obj_cols = []

    for var_column, var in data["variables"].items():
        if var["type"] == "object":
            obj_cols.append(var_column)
    # Récupération des colonnes de type objet

    divs = []

    for col in obj_cols:
        values = data["variables"][col]

        text = f"<b>Colonne :</b> {col}<br>"
        text += f"<b>Type :</b> {values['type']}<br>"
        text += f"<b>Valeurs manquantes :</b> {values['missing values']}<br>"
        text += f"""<b>Pourcentage valeurs manquantes
        :</b> {values['missing values percent column']}%<br>"""

        text += f"<b>Valeurs uniques :</b> {values['unique value']}<br>"
        text += f"<b>Valeurs les plus récurentes :</b> {values['value highest appearance']}<br>"
        text += f"<b>Nombre de valeurs les plus récurentes :</b> {values['nbr of highest app']}<br>"
        text += f"<b>Nombre de récurrence :</b> {values['highest appearance']}<br>"
        text += f"""<b>Récurrence >10%
        :</b> {values['>10% appearance']}<br>"""
        # Traitement des valeurs des colonnes de type objet

        div = Div(text=text, width=400)
        divs.append(div)
        # Ajout des informations

    title = Div(
        text="""<h1 style='text-align: center; color: #000080; font-size: 24px;
        font-weight: bold;'>Informations objet</h1>"""
    )
    return divs, title


def file_property(data):
    """
    Prépare les graphiques des données de type date

    Arguments :
    - data : les données en format json.

    Retourne :
    - divs : Le texte préparé avec les données de data.
    - title : Le titre de la catégorie.
    """
    divs = []
    values = data["file_properties"]
    # Initialisation et récupération des valeurs

    text = f"<b>File name :</b> {values['file_name']}<br>"
    text += f"""<b>Date d'ouverture :
    </b> {values['date']}<br>"""
    text += f"<b>Nombre de lignes :</b> {values['Number of lines']}<br>"
    text += f"<b>Nombre de colonnes :</b> {values['Number of columns']}<br>"
    text += f"""<b>Nom des colonnes :
    </b> {values['Name of columns']}<br>"""
    text += f"""<b>Nombre d'entrées :
    </b> {values['Number of data']}<br>"""
    text += f"""<b>Valeurs manquantes :
    </b> {values['Missing values']}<br>"""
    text += f"""<b>Pourcentage de valeurs manquantes :
    </b> {values['Percentage of missing values']}<br>"""
    text += f"<b>Date de création :</b> {values['Creation date']}<br>"
    # Valeurs principales du fichier

    div = Div(text=text, width=400)
    divs.append(div)
    # Ajout des informations

    title = Div(
        text="""<h1 style='text-align: center; color: black; font-size: 24px;
        font-weight: bold;'>File property</h1>"""
    )
    return divs, title


def dashboard(file, folder_path):
    """
    Renvoie un dashboard préparé avec les fonctions des graphes précédentes

    Arguments :
    - file : le chemin des données en format json.
    - folder_path : chemin du dossier stockage à créer/existant

    Retourne :
    - dashboard : le dashboard du fichier passé
    """
    file_name = os.path.basename(file)
    file_name = os.path.splitext(file_name)[0]
    file_name = os.path.splitext(file_name)[0]
    # Nom du fichier on enlève .json puis le .csv/.xlsx

    with open(file, "r", encoding="utf-8") as json_file:
        data = json.load(json_file)
    # Récupération du fichier .json

    plots_bin, title_bin = binary_graph(data)
    div_bin, title_bin2 = binary_info(data)
    plots_cat, title_cat = categorial_graph(data)
    div_cat, title_cat2 = categorial_info(data)
    # plots_id, title_id = id_graph(data)
    div_id, title_id2 = id_info(data)
    div, title_tab = tableau(data)
    # div_year, title_year = year(data)
    div_date, title_date = date_tab(data)
    div_file, title_file = file_property(data)
    div_obj, title_obj = object_tab(data)
    # Récupération des informations

    column0 = column([title_file, *div_file])
    column1 = column([title_bin, *plots_bin])
    column2 = column([title_bin2, *div_bin])
    column3 = column([title_cat, *plots_cat])
    column4 = column([title_cat2, *div_cat])
    # column5 = column([title_id, *plots_id])
    column5 = column([title_id2, *div_id])
    column6 = column([title_tab, *div])
    # column7 = column([title_year, *div_year])
    column7 = column([title_date, *div_date])
    column8 = column([title_obj, *div_obj])
    # Mise en forme en colonne

    final_dashboard = layout(
        [
            [
                column0,
                column1,
                column2,
                column3,
                column4,
                column5,
                column6,
                column7,
                column8,
            ]
        ],
        sizing_mode="scale_width",
    )

    final_dashboard.background = "#eeeeee"

    output_file(f"{folder_path}/dashboard_{file_name}.html")

    show(final_dashboard)


def analysis(file, folder_create, folder_path, sep=",", folder_name="reports"):
    """
    Convertis le document (csv/xlsx) en dictionnaire json
    et ensuite prépare le dashboard associé à ce dernier.

    Arguments :
    - file : chemin vers le fichier .csv/.xlsx.
    - folder_create : chemin du dossier où placer le dossier stockage
    - folder_path : chemin du dossier stockage à créer/existant
    - sep : le séparateur utilisé dans le fichier, par défaut ",".
    - folder_name (str): Nom du dossier stockage principal , par défaut "reports"
    """

    file_name = os.path.basename(file)
    file_name = os.path.splitext(file_name)[0]
    # Nom du fichier, on garde le nom sans le .json

    json_doc.main(file, folder_create, sep=sep, folder_name=folder_name)
    dashboard(
        f"{folder_path}/{file_name}/{file_name}.json", f"{folder_path}/{file_name}"
    )
