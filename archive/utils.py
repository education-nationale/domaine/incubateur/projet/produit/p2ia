import pandas as pd
import navi.main as navi


fake_data = {
    'matia': {
        'students': {
            'count': 1000,
            'active': 800,
        },
        'count_teacher_active': 3000,
    }
}


def teacher_active() -> pd.DataFrame:
    return pd.DataFrame({
        'tool': ['navi', 'matia'],
        'count': [
            navi.count_teacher_active,
            fake_data['matia']['count_teacher_active']
        ]
    })
