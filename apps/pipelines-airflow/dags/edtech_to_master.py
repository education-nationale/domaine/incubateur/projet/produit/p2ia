from airflow.decorators import dag
from airflow.utils.dates import days_ago
from dags.taskgroup.csv_to_postgres import csv_to_postgres_taskgroup
from dags.taskgroup.execute_postgres_script import execute_sql_taskgroup
from config.edtechs import edtechs


@dag(
    dag_id='edtech_to_master',
    default_args={'start_date': days_ago(1)},
    schedule_interval='@daily',
    catchup=False,
)
def edtech_to_master_dag():
    for edtech, params in edtechs.items():
        csv_to_postgres = csv_to_postgres_taskgroup(
            file_path=params['file_path'],
            db_name=params['db_name'],
        )

        execute_sql = execute_sql_taskgroup(
            path=params['master_path'],
        )

        csv_to_postgres >> execute_sql


dag = edtech_to_master_dag()
