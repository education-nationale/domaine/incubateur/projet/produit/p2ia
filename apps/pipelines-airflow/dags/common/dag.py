import datetime
from dataclasses import dataclass
from dags.common.variable import DEFAULT_RETRIES

from pendulum import duration

@dataclass
class Schedule:
    start_date: datetime.datetime
    schedule_interval: str

DEFAULT_ARGS_COMMON = {
    "retries": DEFAULT_RETRIES,
    "retry_delay": duration(seconds=5),
    # Exponential backoff in order to retry immediately, then later and later
    "retry_exponential_backoff": True,
    # 3 minutes already sounds like a fair delay. 5 minutes (default) is quite long in production
    # before getting alerted.
    "max_retry_delay": duration(minutes=3),
}