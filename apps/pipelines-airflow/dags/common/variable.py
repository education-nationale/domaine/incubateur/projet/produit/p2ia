from enum import Enum

from include.utils import get_env_str, get_env

class RunnerStage(Enum):
    dev = "dev"
    prod = "prod"

def get_common_default_args_retry() -> int:
    configured_value = get_env("COMMON_DEFAULT_ARGS_RETRY", None)
    if configured_value is None:
        # Arbitrary default
        return 3
    else:
        # We want to fail if we cannot parse the string as an int
        return int(configured_value)

#TODO : Réévaluer les variables d'environnement, certaines devraient passer en paramètres
STAGE = get_env_str("STAGE", RunnerStage.dev.value)
DEFAULT_RETRIES = get_common_default_args_retry()
HOST = get_env('HOST')
USER_DB = get_env('USER_DB')
PASSWORD = get_env('PASSWORD')
PORT = get_env('PORT')
DB = get_env('DATABASE')