from datetime import datetime

from airflow import DAG
from airflow.operators.empty import EmptyOperator

default_args = {
    "owner": "airflow",
    "depends_on_past": False,
    "start_date": datetime(2024, 3, 26),
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 1,
}

dag = DAG(
    "test",
    default_args=default_args,
    description="Un DAG simple avec un opérateur vide",
    schedule_interval=None,
    catchup=False,
)

dummy_operator = EmptyOperator(task_id="dummy_task", dag=dag)

dummy_operator