from typing import Any

import pandas as pd

from dags.common.helpers import task_logger

from nuxeo.client import Nuxeo

def read_nuxeo_csv(
    nuxeo_path: str,
    convert_dates: list[str] | None = None,
    convert_datetimes: list[str] | None = None,
    **pandas_kwargs: Any,
) -> pd.DataFrame:
    task_logger.info(f"Downloading file dataset {nuxeo_path}")
    downloaded_filename = nuxeo.documents.get(path=nuxeo_path)
    task_logger.info("Loading downloaded file as a dataset")
    
    df: pd.DataFrame = pd.read_csv(
        downloaded_filename,
        dtype_backend="pyarrow",
        **pandas_kwargs,
    )

    task_logger.info("Dataset loaded")
    return df