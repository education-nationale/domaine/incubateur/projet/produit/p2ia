from sqlalchemy import create_engine, inspect, text

import pandas as pd

from dags.common.variable import HOST, USER_DB, PASSWORD, PORT, DB #TODO: à paramétriser par EdTech
from dags.common.helpers import task_logger

class PostgresEngine:
    def __init__(self):
        self.db_url = f'postgresql://{USER_DB}:{PASSWORD}@{HOST}:{PORT}/{DB}'

    def get_engine(self):
        return create_engine(self.db_url)

    def get_inspector(self):
        engine = self.get_engine()
        return inspect(engine)

    def run_query(self, query: str):
        engine = self.get_engine()
        with engine.begin() as conn:
            result = conn.execute(text(query))
        return result

def load_df_to_table(
    df: pd.DataFrame,
    postgres_db: str,
    postgres_schema: str,
    postgres_table: str,
    replace: bool = False,
    # postgres_write_options: TODO
):
    
    task_logger.info(f"Dumping dataframe to table={postgres_db}.{postgres_schema}.{postgres_table}")
    
    eng = PostgresEngine().get_engine()
    df.to_sql(
            name=postgres_schema, con=eng, if_exists='append', index=False, schema=env
        )
    