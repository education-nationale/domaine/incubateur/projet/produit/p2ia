from airflow import DAG
from airflow.decorators import dag, task_group
from airflow.operators.python import PythonOperator
from load import load_to_postgres
import os


@task_group
def csv_to_postgres_taskgroup(
        file_path: str,
        db_name: str,
        env: str = 'raw',
        chunksize: int = 100000):
    task_load_to_postgres = PythonOperator(
        task_id='load_to_postgres',
        python_callable=load_to_postgres,
        op_kwargs={
            'data': file_path,
            'db_name': db_name,
            'env': env,
            'chunksize': chunksize,
            'path': os.path.dirname(file_path)
        },
    )

    return task_load_to_postgres
