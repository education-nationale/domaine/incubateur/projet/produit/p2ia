from airflow.decorators import dag, task_group
from airflow.operators.python import PythonOperator
from bdd_commune.script.data.engineering.exe_postgres import exe_sql_postgres


@task_group
def execute_sql_taskgroup(path: str, **kwargs):
    """
    TaskGroup to handle the execution of a SQL script.
    """
    task_execute_sql_script = PythonOperator(
        task_id='execute_sql_script',
        python_callable=exe_sql_postgres,
        op_kwargs={
            'path': path,
            **kwargs
        },
    )

    return task_execute_sql_script
