from dataclasses import dataclass

from dags.common.variable import STAGE, RunnerStage

# The goal of those constants is to track what remains to do.
TODO_STRING = ""
TODO_LIST = []  # type: ignore


@dataclass
class EdTech:
    ID: str
    NAME: str
    bucket: str
    postgres_secret: str


def get_postgres_secret(TODO_STRING: str) -> str:  # pragma: nocover
    if STAGE == RunnerStage.prod.value:
        return TODO_STRING
    else:
        return TODO_STRING
    
def get_nuxeo_secret(TODO_STRING: str) -> str:  # pragma: nocover
    if STAGE == RunnerStage.prod.value:
        return TODO_STRING
    else:
        return TODO_STRING