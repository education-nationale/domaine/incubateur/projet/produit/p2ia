from dags.conf.model import (
    EdTech,
    get_nuxeo_secret,
    get_postgres_secret
)

LALILO = EdTech(
    ID="lalilo",
    NAME="lalilo",
    nuxeo_secret=get_nuxeo_secret("lalilo"),
    postgres_secret=get_postgres_secret("lalilo")
    )

#     "data": "(path)/lalilo.csv",
#     "master_path": "bdd_commune/script/data/engineering/master/lalilo.sql",
# 

