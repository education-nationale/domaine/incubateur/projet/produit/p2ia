from dags.conf.model import (
    EdTech,
    get_nuxeo_secret,
    get_postgres_secret
)

EVIDENCE_B = EdTech(
    ID="evidenceb",
    NAME="evidenceb",
    nuxeo_secret=get_nuxeo_secret("evidenceb"),
    postgres_secret=get_postgres_secret("evidenceb")
    )

