from dataclasses import dataclass
from typing import Any, Literal

from airflow.models import BaseOperator
from airflow.utils.context import Context

from dags.connector.nuxeo.core import read_nuxeo_csv

@dataclass
class PandasReadCsvOptions:
    encoding: str = "utf-8"
    sep: str = ";"
    decimal: str = "."
    quotechar: str = "|"
    compression: Literal[
        "infer", "gzip", "bz2", "zip", "xz", "zstd", "tar"
    ] | None = "infer"
    dtype: dict[str, str] | None = None
    index_col: bool | None = None
    names: list[str] | None = None
    convert_dates: list[str] | None = None
    convert_datetimes: list[str] | None = None


class NuxeoCsvToXcom(BaseOperator):
    def __init__(
        self,
        xcom_key: str,
        nuxeo_path: str,
        read_csv_options: PandasReadCsvOptions = PandasReadCsvOptions(),
        **kwargs: Any,
    ):
        super().__init__(**kwargs)
        self.xcom_key = xcom_key
        self.nuxeo_path = nuxeo_path
        self.read_csv_options = read_csv_options


    def execute(self, context: Context) -> None:
        self.log.info(f"The columns of the csv file are the following {self.read_csv_options.names}")

        df = read_nuxeo_csv(
            nuxeo_path=self.nuxeo_path,
            encoding=self.read_csv_options.encoding,
            sep=self.read_csv_options.sep,
            decimal=self.read_csv_options.decimal,
            quotechar=self.read_csv_options.quotechar,
            compression=self.read_csv_options.compression,
            dtype=self.read_csv_options.dtype,
            index_col=self.read_csv_options.index_col,
            names=self.read_csv_options.names,
            convert_dates=self.read_csv_options.convert_dates,
            convert_datetimes=self.read_csv_options.convert_datetimes,
            chunksize=self.read_csv_options.chunksize
        )
        
        self.xcom_push(
            context=context,
            key=self.xcom_key,
            value=df,
        )


