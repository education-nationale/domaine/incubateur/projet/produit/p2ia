from dataclasses import dataclass
from typing import Any, Literal

from airflow.models import BaseOperator
from airflow.utils.context import Context

from dags.connector.postgres.core import load_df_to_table

# @dataclass
# class XcomToPostgresOptions: #TODO
    # specify incremental writing, indexes, partioning, ...
    

class XcomToPostgres(BaseOperator): 
    def __init__(
        self,
        xcom_key: str,
        #TODO: revoir les paramètres de cette classe
        postgres_db: str,
        postgres_schema: str,
        postgres_table: str, 
        # postgres_write_options: XcomToPostgresOptions = XcomToPostgresOptions(), #TODO 
        **kwargs: Any,
    ):
        super().__init__(**kwargs)
        self.xcom_key = xcom_key
        self.postgres_db = postgres_db
        self.postgres_schema = postgres_schema
        self.postgres_table = postgres_table
        # self.postgres_write_options = postgres_write_options
        
    def execute(self, context: Context) -> None:
        df = self.xcom_pull(
            context=context,
            key=self.xcom_key,
        )

        load_df_to_table(df,
                         #TODO: paramétriser
                         )






