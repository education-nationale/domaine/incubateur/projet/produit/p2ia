import datetime

from airflow import DAG
from airflow.operators.empty import EmptyOperator
from airflow.decorators import dag
from dags.common.dag import DEFAULT_ARGS_COMMON
from dags.operator.csv_to_xcom import NuxeoCsvToXcom
from dags.operator.xcom_to_postgres import XcomToPostgres
# from dags.taskgroup.csv_to_postgres import csv_to_postgres_taskgroup
# from dags.taskgroup.execute_postgres_script import execute_sql_taskgroup
from dags.dag_extraction.extraction_conf import (
    ExtractionParameters, 
    DAG_PARAMETRIZATION,
    )

# The goal of those constants is to track what remains to do.
TODO_STRING = ""
TODO_LIST = []  # type: ignore

def generate_dag(dag_id: str, parametrization: ExtractionParameters) -> DAG:
    dag: DAG
    with DAG(
        dag_id=dag_id,
        start_date=parametrization.SCHEDULE.start_date,
        schedule_interval=parametrization.SCHEDULE.schedule_interval,
        default_args=DEFAULT_ARGS_COMMON,
        tags=[
            parametrization.EDTECH.ID,
        ],
        catchup=False,
    ) as dag:
        
        start = EmptyOperator(task_id="start")
        end = EmptyOperator(task_id="end")
        
        load_csv = NuxeoCsvToXcom(
            file_path=TODO_STRING,
            xcom_key=TODO_STRING
        )

        upload_postgres = XcomToPostgres(
            table_name=TODO_STRING,
            xcom_key=TODO_STRING,
            postgres_db=TODO_STRING,
            postgres_schema=TODO_STRING,
            postgres_table=TODO_STRING,
        )

        start >> load_csv
        load_csv >> upload_postgres
        upload_postgres >> end

    return dag


for parametrization in DAG_PARAMETRIZATION:
    if not parametrization.active:
        continue
    dag_id = f"extraction_{parametrization.EDTECH.NAME}"
    d = generate_dag(dag_id, parametrization)
    globals()[dag_id] = d

    if __name__ == "__main__":
        d.test()
