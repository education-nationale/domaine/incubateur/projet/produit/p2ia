from dataclasses import dataclass, field

import pendulum

from dags.common.dag import Schedule
from dags.conf.evidenceb.edtech import EVIDENCE_B
from dags.conf.model import EdTech

@dataclass
class ExtractionParameters:
    EDTECH: EdTech
    SCHEDULE: Schedule = field(
        default_factory=lambda: ExtractionParameters.get_default_schedule()
    )
    active: bool = True

    @staticmethod
    def get_default_schedule() -> Schedule:
        return Schedule(
            start_date=pendulum.datetime(2024, 1, 1, tz="Europe/Paris"),
            schedule_interval="0 8 * * *",
        )

extraction_evidenceb = ExtractionParameters(
    EDTECH=EVIDENCE_B,
)

DAG_PARAMETRIZATION = [
    extraction_evidenceb,
]
