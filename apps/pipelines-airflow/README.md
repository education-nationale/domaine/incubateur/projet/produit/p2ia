# Airflow

## DAGs du projet

### DAG Extraction des donnéees : dag_extraction/

Tâches :

- Tâche 1 : Extraction
  tâche qui se connecte à Nuxeo (et autres données) via API pour télécharger les fichiers .csv ou .json et les stocker dans le XCOM
- Tâche 2 : Chargement dans Postgres (raw)
  Charger les données brutes dans une table Postgres raw sans transformation initiale. Cela permet de conserver une copie exacte des données sources.

### DAG Tranformation des donnéees : dag_transformation/

Tâches :

- Exécution des modèles DBT - Nettoyage
  Exécute des modèles DBT pour transformer les données brutes et les stocker dans des tables nettoyées.
- Exécution des modèles DBT - Données analytiques
  Exécute les modèles DBT pour générer les tables analytiques qui seront utilisées dans Superset. Ces tables seront stockées dans la couche analytics de votre base de données.

### DAG Mise à jour du Tableau de Bord : dag_dashboard/

- Rafraîchissement Superset
  Une fois les transformations effectuées, déclenche un rafraîchissement ou notifie Superset pour mettre à jour les tableaux de bord si nécessaire.

## Bonnes pratiques Airflow

Quelques bonnes pratiques suggérées :

- Modularité des DAGs
  Évitez de tout regrouper dans un seul DAG. Divisez les tâches en plusieurs DAGs indépendants (comme indiqué ci-dessus) pour une meilleure maintenabilité et un débogage simplifié.
- Utilisation des XComs et variables
  Utilisez les XComs pour échanger des informations entre tâches (Task) au sein d'un même DAG, et les Variables d'Airflow pour stocker des configurations partagées ou des secrets.
- Gestion des dépendances  
  Assurez-vous que chaque tâche (Task) est bien définie avec ses dépendances. Par exemple, une tâche (Task) de transformation DBT ne doit démarrer qu'après le chargement des données brutes.
- Idempotence des tâches
  Chaque tâche (Task) devrait pouvoir être exécutée plusieurs fois sans causer de problèmes (ce qui signifie par exemple que les transformations de DBT doivent pouvoir être rejouées sans impact).
- Alerting & Monitoring
  Utilisez les mécanismes d’alerte d’Airflow pour recevoir des notifications en cas d'échec de certaines tâches critiques.
- Réglage de la fréquence d'éxécution
  Réglez la fréquence d'exécution des DAGs selon les besoins métier (par exemple, quotidiennement, toutes les heures, etc.).

## Utilisation de DBT dans Airflow

DBT peut être exécuté depuis Airflow via :

- DBT Cloud en utilisant l'opérateur spécifique à DBT Cloud (`DbtCloudRunJobOperator`) pour exécuter des jobs DBT hébergés.
- Exécution locale de DBT en utilisant un `BashOperator` pour exécuter les commandes DBT en ligne de commande, par exemple : `dbt run --full-refresh`

## Structure de la base de données Postgres

- Couche de données brutes : Stockez les données brutes ici sans modification.
- Couche de données nettoyées : Transformez les données pour les nettoyer et standardiser.
- Couche de données analytiques : Créez des tables optimisées pour l’analyse et les requêtes par Superset.
